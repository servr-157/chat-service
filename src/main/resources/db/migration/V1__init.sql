CREATE TABLE attachments
(
    id               BIGINT AUTO_INCREMENT NOT NULL,
    message_id       BIGINT                NOT NULL,
    file_name        VARCHAR(255)          NOT NULL,
    file_path        TEXT                  NOT NULL,
    compression_path TEXT                  NOT NULL,
    type             VARCHAR(255)          NOT NULL,
    created_at       datetime              NULL,
    updated_at       datetime              NULL,
    deleted_at       datetime              NULL,
    CONSTRAINT pk_attachments PRIMARY KEY (id)
);

CREATE TABLE conversations
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    title        VARCHAR(255)          NULL,
    type         VARCHAR(255)          NOT NULL,
    avatar       TEXT                  NULL,
    last_message LONGTEXT              NULL,
    created_by   VARCHAR(64)           NULL,
    updated_by   VARCHAR(64)           NULL,
    created_at   datetime              NULL,
    updated_at   datetime              NULL,
    deleted_at   datetime              NULL,
    CONSTRAINT pk_conversations PRIMARY KEY (id)
);

CREATE TABLE messages
(
    id              BIGINT AUTO_INCREMENT NOT NULL,
    user_id         VARCHAR(255)          NOT NULL,
    conversation_id BIGINT                NOT NULL,
    content         TEXT                  NOT NULL,
    created_at      datetime              NULL,
    updated_at      datetime              NULL,
    deleted_at      datetime              NULL,
    CONSTRAINT pk_messages PRIMARY KEY (id)
);

CREATE TABLE permissions
(
    id     BIGINT AUTO_INCREMENT NOT NULL,
    name   VARCHAR(255)          NOT NULL,
    module VARCHAR(255)          NOT NULL,
    CONSTRAINT pk_permissions PRIMARY KEY (id)
);

CREATE TABLE roles
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    name       VARCHAR(255)          NOT NULL,
    code       VARCHAR(255)          NOT NULL,
    created_at datetime              NULL,
    updated_at datetime              NULL,
    deleted_at datetime              NULL,
    CONSTRAINT pk_roles PRIMARY KEY (id)
);

CREATE TABLE roles_has_permissions
(
    permission_id BIGINT NOT NULL,
    role_id       BIGINT NOT NULL
);

CREATE TABLE user_conversation
(
    id              BIGINT AUTO_INCREMENT NOT NULL,
    user_id         VARCHAR(255)          NOT NULL,
    conversation_id BIGINT                NOT NULL,
    role_id         BIGINT                NOT NULL,
    created_at      datetime              NULL,
    updated_at      datetime              NULL,
    deleted_at      datetime              NULL,
    CONSTRAINT pk_user_conversation PRIMARY KEY (id)
);

CREATE TABLE users
(
    user_id    VARCHAR(255) NOT NULL,
    name       VARCHAR(255) NULL,
    avatar     VARCHAR(255) NULL,
    created_at datetime     NULL,
    updated_at datetime     NULL,
    deleted_at datetime     NULL,
    CONSTRAINT pk_users PRIMARY KEY (user_id)
);

ALTER TABLE roles
    ADD CONSTRAINT uc_roles_name UNIQUE (name);

CREATE UNIQUE INDEX idx_role_code ON roles (code);

ALTER TABLE attachments
    ADD CONSTRAINT FK_ATTACHMENTS_ON_MESSAGE FOREIGN KEY (message_id) REFERENCES messages (id);

ALTER TABLE messages
    ADD CONSTRAINT FK_MESSAGES_ON_CONVERSATION FOREIGN KEY (conversation_id) REFERENCES conversations (id);

CREATE INDEX idx_conversation_id ON messages (conversation_id);

ALTER TABLE messages
    ADD CONSTRAINT FK_MESSAGES_ON_USER FOREIGN KEY (user_id) REFERENCES users (user_id);

CREATE INDEX idx_user_id ON messages (user_id);

ALTER TABLE user_conversation
    ADD CONSTRAINT FK_USER_CONVERSATION_ON_CONVERSATION FOREIGN KEY (conversation_id) REFERENCES conversations (id);

ALTER TABLE user_conversation
    ADD CONSTRAINT FK_USER_CONVERSATION_ON_ROLE FOREIGN KEY (role_id) REFERENCES roles (id);

ALTER TABLE user_conversation
    ADD CONSTRAINT FK_USER_CONVERSATION_ON_USER FOREIGN KEY (user_id) REFERENCES users (user_id);

ALTER TABLE roles_has_permissions
    ADD CONSTRAINT fk_rolhasper_on_permission_entity FOREIGN KEY (permission_id) REFERENCES permissions (id);

ALTER TABLE roles_has_permissions
    ADD CONSTRAINT fk_rolhasper_on_role_entity FOREIGN KEY (role_id) REFERENCES roles (id);