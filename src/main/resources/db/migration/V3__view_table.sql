drop view if exists conversation_view;
create view conversation_view as
select c.id,
       if(c.type = 'PRIVATE', u.name, c.title) as title,
       if(c.type = 'PRIVATE', u.avatar, c.avatar) as avatar,
       c.type,
       u.user_id,
       c.last_message,
       c.created_at,
       c.created_by,
       c.updated_at,
       c.updated_by,
       c.deleted_at
from conversations c
         inner join user_conversation uc on c.id = uc.conversation_id
         left join users u on u.user_id = uc.user_id;