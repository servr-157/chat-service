insert into roles (id, created_at, updated_at, deleted_at, name, code)
    value
    (1, now(), now(), null, 'Owner', 'owner'),
    (2, now(), now(), null, 'Admin', 'admin'),
    (3, now(), now(), null, 'User', 'user');

insert into conversations (id, created_at, updated_at, deleted_at, title, type, avatar, created_by, updated_by)
    value
    (null, now(), now(), null, null, 'PRIVATE', null, 'admin', 'admin'),
    (null, now(), now(), null, 'Group-A', 'GROUP', 'https://images.squarespace-cdn.com/content/v1/5fe4caeadae61a2f19719512/c25ae1ed-f0a3-45db-b396-9fd9d3a2dc95/Zoro', 'admin', 'admin'),
    (null, now(), now(), null, 'Group-B', 'GROUP', 'https://i.pinimg.com/236x/68/47/a4/6847a40001c6aa39c4d7b6ad089b736a.jpg', 'admin', 'admin'),
    (null, now(), now(), null, 'Group-C', 'GROUP', 'https://static1.cbrimages.com/wordpress/wp-content/uploads/2023/09/ace-and-strawhat-crew-one-piece.jpg', 'admin', 'admin'),
    (null, now(), now(), null, 'Group-D', 'GROUP', 'https://hips.hearstapps.com/hmg-prod/images/screen-shot-2023-09-15-at-4-19-31-pm-6504bc60249ac.png?crop=0.8333333333333334xw:1xh;center,top&resize=1200:*', 'admin', 'admin'),
    (null, now(), now(), null, 'Group-E', 'GROUP', 'https://www.dexerto.com/cdn-cgi/image/width=3840,quality=60,format=auto/https://editors.dexerto.com/wp-content/uploads/2023/08/16/one-piece-gear-5-luffy.jpeg', 'admin', 'admin')
;

insert into users (user_id, name, avatar, created_at, updated_at, deleted_at)
    value
    ('123', 'kimleng','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4rbk5exRvIWzIcjGdZKnV1ZMWY5XMXCCjYA&s', now(), now(), null),
    ('1234', 'yuth', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRv_m3dTkPV53pNEz4unRlkP1b36bpe3atSXw&s', now(), now(), null),
    ('12345', 'soklyly', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZxF2jO-LIVCi9fJeeW4zdBNp6dMhPG__1Yg&s', now(), now(), null),
    ('123456', 'yihaov', 'https://hips.hearstapps.com/hmg-prod/images/screen-shot-2023-09-15-at-4-19-31-pm-6504bc60249ac.png?crop=1xw:0.9xh;center,top&resize=1200:*', now(), now(), null);