package com.kilo.service.chat.infrastructure.persistence.database.attachment;

import com.kilo.service.chat.domain.attachment.entity.Attachment;
import com.kilo.service.chat.domain.attachment.repository.AttachmentRepository;
import com.kilo.service.chat.infrastructure.persistence.database.attachment.entity.AttachmentEntity;
import com.kilo.service.chat.infrastructure.persistence.database.attachment.mapper.AttachmentDBMapper;
import com.kilo.service.chat.infrastructure.persistence.database.attachment.repository.AttachmentJPARepository;
import com.kilo.service.chat.infrastructure.persistence.database.column.AttachmentTypeEnum;
import com.kilo.service.chat.infrastructure.persistence.database.message.repository.MessageJPARepository;
import com.kilo.service.chat.share.entity.domain.Paging;
import com.kilo.service.chat.share.utility.PageNumberUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttachmentDBRepository implements AttachmentRepository {
    private final AttachmentDBMapper mapper;
    private final AttachmentJPARepository attachmentRepository;
    private final MessageJPARepository messageRepository;

    @Override
    public Paging<Attachment> filter(String filename, String type, Pageable pageable){
        Page<AttachmentEntity> entityPage = attachmentRepository.findByFileNameLikeAndTypeAndDeletedAtNull(filename,AttachmentTypeEnum.valueOf(type),pageable);
        return Paging.<Attachment>builder()
                .items(entityPage.map(mapper::from).getContent())
                .size(entityPage.getSize())
                .page(PageNumberUtility.out(entityPage.getNumber()))
                .total(entityPage.getNumberOfElements())
                .totalPages(entityPage.getTotalPages())
                .build();
    }

    @Override
    public Attachment save(Attachment attachment) {
//        HeaderCommand headerCommand = RequestHeaderUtility.getHeaderCommand();
        AttachmentEntity attachmentEntity = mapper.to(attachment);
        attachment.setMessage(messageRepository.getReferenceById(attachment.getMessage().getId()));
        return mapper.from(attachmentRepository.save(attachmentEntity));
    }

    @Override
    public Optional<Attachment> findById(Long id) {
        Optional<AttachmentEntity> attachmentEntity = attachmentRepository.findById(id);
        return attachmentEntity.map(mapper::from);
    }
}
