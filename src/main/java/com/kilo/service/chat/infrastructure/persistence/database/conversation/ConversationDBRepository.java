package com.kilo.service.chat.infrastructure.persistence.database.conversation;

import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.conversation.entity.ConversationView;
import com.kilo.service.chat.domain.conversation.repository.ConversationRepository;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationViewEntity;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.mapper.ConversationDBMapper;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.repository.ConversationEntityRepository;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.repository.ConversationViewRepository;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.specification.ConversationSpecification;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationEntity;
import com.kilo.service.chat.share.entity.domain.Paging;
import com.kilo.service.chat.share.utility.PageNumberUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConversationDBRepository implements ConversationRepository {

    private final ConversationEntityRepository conversationEntityRepository;
    private final ConversationViewRepository conversationViewRepository;
    private final ConversationDBMapper mapper;

    @Transactional(readOnly = true)
    @Override
    public Paging<ConversationView> findAll(int page, int size, String keyword, String type, String userId) {

        Page<ConversationViewEntity> entityPage = conversationViewRepository.findAll(
                ConversationSpecification.filter(keyword, type, userId),
                PageRequest.of(PageNumberUtility.in(page), size, Sort.by(Sort.Direction.DESC, "updatedAt"))
        );

        List<ConversationView> conversations = entityPage.stream().map(mapper::from).toList();
        return Paging.<ConversationView>builder()
                .items(conversations)
                .page(PageNumberUtility.out(entityPage.getNumber()))
                .size(entityPage.getSize())
                .total(entityPage.getNumberOfElements())
                .totalPages(entityPage.getTotalPages())
                .build();
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<Conversation> findById(Long id) {
        Optional<ConversationEntity> conversationEntity = conversationEntityRepository.findByIdAndDeletedAtNull(id);
        return conversationEntity.map(mapper::from);
    }

    @Transactional
    @Override
    public Optional<ConversationView> findByConversationId(String userId, Long conversationId) {
        Optional<ConversationViewEntity> userConversation = conversationViewRepository.findAll(ConversationSpecification.findConversationsById(userId, conversationId)).stream().findFirst();
        return userConversation.map(mapper::from);
    }

    @Transactional
    @Override
    public Conversation save(Conversation conversation) {
        ConversationEntity entity = mapper.from(conversation);
        return mapper.from(conversationEntityRepository.save(entity));
    }
}
