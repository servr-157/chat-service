package com.kilo.service.chat.infrastructure.persistence.database.message.mapper;

import com.kilo.service.chat.domain.message.entity.Message;
import com.kilo.service.chat.infrastructure.persistence.database.message.entity.MessageEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface MessageDBMapper {

    MessageEntity from(Message message);

    @Mapping(target = "user", source = "user")
    @Mapping(target = "conversation", source = "conversation")
    Message from(MessageEntity messageEntity);
}
