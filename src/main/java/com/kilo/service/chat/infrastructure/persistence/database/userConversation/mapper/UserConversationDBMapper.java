package com.kilo.service.chat.infrastructure.persistence.database.userConversation.mapper;

import com.kilo.service.chat.domain.userConversation.entity.UserConversation;
import com.kilo.service.chat.infrastructure.persistence.database.userConversation.entity.UserConversationEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Arrays;
import java.util.List;

@Mapper
public interface UserConversationDBMapper {
    @Mapping(target = "userId",source = "user.userId")
    @Mapping(target = "conversationId",source = "conversations.id")
    @Mapping(target = "roleId",source = "role.id")
    UserConversation from(UserConversationEntity userConversationEntity);

    @Mapping(target = "user.userId",source = "userId")
    @Mapping(target = "conversations.id",source = "conversationId")
    @Mapping(target = "role.id",source = "roleId")
    UserConversationEntity to(UserConversation userConversation);

    default String map(List<String> userIds) {
        return (userIds != null && !userIds.isEmpty()) ? String.join(",", userIds) : null;
    }

    default List<String> map(String userIds) {
        return (userIds != null && !userIds.isEmpty()) ? Arrays.asList(userIds.split(",")) : null;
    }
}
