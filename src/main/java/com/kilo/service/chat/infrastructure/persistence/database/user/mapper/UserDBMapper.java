package com.kilo.service.chat.infrastructure.persistence.database.user.mapper;

import com.kilo.service.chat.domain.user.entity.User;
import com.kilo.service.chat.infrastructure.persistence.database.user.entity.UserEntity;
import org.mapstruct.Mapper;

@Mapper
public interface UserDBMapper {

    UserEntity from(User user);

    User from(UserEntity userEntity);


}
