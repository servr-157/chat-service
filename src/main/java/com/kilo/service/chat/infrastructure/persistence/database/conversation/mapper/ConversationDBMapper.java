package com.kilo.service.chat.infrastructure.persistence.database.conversation.mapper;

import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.conversation.entity.ConversationView;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationEntity;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationViewEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface ConversationDBMapper {

    ConversationEntity from(Conversation conversation);

    @Mapping(target = "type", source = "type")
    Conversation from(ConversationEntity conversationEntity);

    @Mapping(target = "lastMessage", source = "lastMessage")
    @Mapping(target = "type", source = "type")
    ConversationView from(ConversationViewEntity entity);
}
