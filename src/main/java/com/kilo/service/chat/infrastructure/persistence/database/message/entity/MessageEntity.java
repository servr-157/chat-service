package com.kilo.service.chat.infrastructure.persistence.database.message.entity;

import com.kilo.service.chat.infrastructure.persistence.database.attachment.entity.AttachmentEntity;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationEntity;
import com.kilo.service.chat.infrastructure.persistence.database.user.entity.UserEntity;
import com.kilo.service.chat.share.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "messages", indexes = {
    @Index(name = "idx_conversation_id", columnList = "conversation_id"),
    @Index(name = "idx_user_id", columnList = "user_id")
})
@Getter
@Setter
public class MessageEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "conversation_id", nullable = false)
    private ConversationEntity conversation;

    @Column(name = "content", nullable = false)
    private String content;

    @OneToMany(mappedBy = "message")
    private List<AttachmentEntity> attachments;

}
