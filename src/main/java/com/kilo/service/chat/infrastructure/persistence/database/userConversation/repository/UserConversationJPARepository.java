package com.kilo.service.chat.infrastructure.persistence.database.userConversation.repository;

import com.kilo.service.chat.infrastructure.persistence.database.userConversation.entity.UserConversationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserConversationJPARepository extends JpaRepository<UserConversationEntity, Long>, JpaSpecificationExecutor<UserConversationEntity> {
    @Query("""
        select u from UserConversationEntity u
        where (:userId is null or u.user.userId like concat('%',:userId,'%'))
        and (:conversationId is null or u.conversations.id = :conversationId)
        and u.deletedAt is null
        order by u.createdAt desc""")
    Page<UserConversationEntity> findByUserIdAndConversationId(
            @Param("userId") String userId ,
            @Param("conversationId") Long conversationId,
            Pageable pageable);

    @Query("""
            select (count(u) > 0) from UserConversationEntity u
            where u.conversations.id = :conversationId and u.user.userId = :userId""")
    boolean existsByConversations_IdEqualsAndUser_UserIdContains(@Param("conversationId") Long conversationId, @Param("userId") String userId);



}