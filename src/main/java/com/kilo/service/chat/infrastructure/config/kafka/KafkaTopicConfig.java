package com.kilo.service.chat.infrastructure.config.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Value("${spring.kafka.topic.message-topic}")
    private String chatTopic;

    @Bean
    public NewTopic chatTopic() {
        return TopicBuilder
                .name(chatTopic)
                .build();
    }
}
