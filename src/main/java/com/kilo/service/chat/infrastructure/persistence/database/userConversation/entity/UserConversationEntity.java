package com.kilo.service.chat.infrastructure.persistence.database.userConversation.entity;

import com.kilo.service.chat.infrastructure.persistence.database.role.entity.RoleEntity;
import com.kilo.service.chat.share.entity.BaseEntity;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationEntity;
import com.kilo.service.chat.infrastructure.persistence.database.user.entity.UserEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "user_conversation")
@Getter
@Setter
public class UserConversationEntity extends BaseEntity {

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "conversation_id", nullable = false)
    private ConversationEntity conversations;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "role_id", nullable = false)
    private RoleEntity role;
}
