package com.kilo.service.chat.infrastructure.persistence.database.role.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kilo.service.chat.infrastructure.persistence.database.permissions.entity.PermissionEntity;
import com.kilo.service.chat.infrastructure.persistence.database.userConversation.entity.UserConversationEntity;
import com.kilo.service.chat.share.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "roles", indexes = {
        @Index(name = "idx_role_code", columnList = "code", unique = true)
})
@Getter
@Setter
public class RoleEntity extends BaseEntity {

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "code", nullable = false)
    private String code;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "roles_has_permissions",
            joinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "permission_id", referencedColumnName = "id"))
    @JsonProperty("permissions")
    private List<PermissionEntity> permissionEntities = new ArrayList<>();

    @OneToMany(mappedBy = "role")
    private Set<UserConversationEntity> userConversations;
}
