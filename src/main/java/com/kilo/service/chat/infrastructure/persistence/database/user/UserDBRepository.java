package com.kilo.service.chat.infrastructure.persistence.database.user;

import com.kilo.service.chat.domain.user.entity.User;
import com.kilo.service.chat.domain.user.repository.UserRepository;
import com.kilo.service.chat.infrastructure.persistence.database.user.entity.UserEntity;
import com.kilo.service.chat.infrastructure.persistence.database.user.mapper.UserDBMapper;
import com.kilo.service.chat.infrastructure.persistence.database.user.repository.UserEntityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDBRepository implements UserRepository {

    private final UserEntityRepository userEntityRepository;
    private final UserDBMapper mapper;

    @Transactional(readOnly = true)
    @Override
    public Optional<User> findById(String userId) {
        Optional<UserEntity> userEntity = userEntityRepository.findByUserIdAndDeletedAtNull(userId);
        return userEntity.map(mapper::from);
    }

    @Override
    public void save(User user) {
        userEntityRepository.save(mapper.from(user));
    }

    @Override
    public List<User> findByIds(List<String> userIds) {
        return userEntityRepository.findAllById(userIds).stream().map(mapper::from).collect(Collectors.toList());
    }
}
