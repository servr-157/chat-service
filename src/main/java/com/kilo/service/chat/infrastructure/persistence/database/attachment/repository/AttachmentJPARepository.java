package com.kilo.service.chat.infrastructure.persistence.database.attachment.repository;

import com.kilo.service.chat.infrastructure.persistence.database.attachment.entity.AttachmentEntity;
import com.kilo.service.chat.infrastructure.persistence.database.column.AttachmentTypeEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AttachmentJPARepository extends JpaRepository<AttachmentEntity, Long> {

    @Query("""
            select a from AttachmentEntity a
            where (:fileName is null or a.fileName like concat('%',:fileName,'%'))
            and (:type = 'all' or a.type = :type)
            and a.deletedAt is null
            order by a.createdAt desc""")
    Page<AttachmentEntity> findByFileNameLikeAndTypeAndDeletedAtNull(
            @Param("fileName") String fileName,
            @Param("type") AttachmentTypeEnum type,
            Pageable pageable);
}