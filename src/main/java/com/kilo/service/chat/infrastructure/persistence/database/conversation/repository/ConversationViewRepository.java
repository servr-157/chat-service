package com.kilo.service.chat.infrastructure.persistence.database.conversation.repository;

import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ConversationViewRepository extends JpaRepository<ConversationViewEntity, Long>, JpaSpecificationExecutor<ConversationViewEntity> {
}