package com.kilo.service.chat.infrastructure.persistence.database.conversation.repository;

import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface ConversationEntityRepository extends JpaRepository<ConversationEntity, Long>, JpaSpecificationExecutor<ConversationEntity> {

    Optional<ConversationEntity> findByIdAndDeletedAtNull(Long id);

}