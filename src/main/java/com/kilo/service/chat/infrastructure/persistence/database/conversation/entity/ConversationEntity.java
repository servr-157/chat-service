package com.kilo.service.chat.infrastructure.persistence.database.conversation.entity;

import com.kilo.service.chat.infrastructure.persistence.database.column.ConversationTypeEnum;
import com.kilo.service.chat.infrastructure.persistence.database.message.entity.MessageEntity;
import com.kilo.service.chat.infrastructure.persistence.database.userConversation.entity.UserConversationEntity;
import com.kilo.service.chat.share.entity.BaseEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Entity
@Table(name = "conversations")
@Setter
@Getter
public class ConversationEntity extends BaseEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "last_message")
    private String lastMessage;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private ConversationTypeEnum type;

    @OneToMany(mappedBy = "conversation")
    private List<MessageEntity> messages;

    @Size(max = 64)
    @Column(name = "created_by", length = 64)
    private String createdBy;

    @Size(max = 64)
    @Column(name = "updated_by", length = 64)
    private String updatedBy;

    @OneToMany(mappedBy = "conversations")
    private Set<UserConversationEntity> userConversations;

}
