package com.kilo.service.chat.infrastructure.persistence.database.conversation.entity;

import com.kilo.service.chat.infrastructure.persistence.database.column.ConversationTypeEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import java.time.Instant;

/**
 * Mapping for DB view
 */
@Getter
@Setter
@Entity
@Immutable
@Table(name = "conversation_view")
public class ConversationViewEntity {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "title")
    private String title;

    @Column(name = "last_message")
    private String lastMessage;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private ConversationTypeEnum type;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "created_by", length = 64)
    private String createdBy;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "updated_by", length = 64)
    private String updatedBy;
}