package com.kilo.service.chat.infrastructure.persistence.database.column;

public enum ConversationTypeEnum {

    PRIVATE,
    GROUP
}
