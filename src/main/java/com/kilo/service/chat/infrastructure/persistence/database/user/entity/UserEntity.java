package com.kilo.service.chat.infrastructure.persistence.database.user.entity;

import com.kilo.service.chat.infrastructure.persistence.database.message.entity.MessageEntity;
import com.kilo.service.chat.infrastructure.persistence.database.userConversation.entity.UserConversationEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;
import java.util.Set;

@Entity
@Table(name = "users")
@Getter
@Setter
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "name")
    private String name;

    @Column(name = "avatar")
    private String avatar;

    @CreationTimestamp
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Instant createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Instant updatedAt;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @OneToMany(mappedBy = "user")
    private Set<MessageEntity> messages;

    @OneToMany(mappedBy = "user",fetch = FetchType.EAGER)
    private Set<UserConversationEntity> userConversations;

}
