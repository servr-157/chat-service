package com.kilo.service.chat.infrastructure.persistence.database.role;

import com.kilo.service.chat.domain.role.entity.Role;
import com.kilo.service.chat.domain.role.repository.RoleRepository;
import com.kilo.service.chat.infrastructure.persistence.database.role.entity.RoleEntity;
import com.kilo.service.chat.infrastructure.persistence.database.role.mapper.RoleDBMapper;
import com.kilo.service.chat.infrastructure.persistence.database.role.repository.RoleJPARepository;
import com.kilo.service.chat.share.entity.domain.Paging;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
@Service
@RequiredArgsConstructor
public class RoleDBRepository implements RoleRepository {
    private final RoleDBMapper roleDBMapper;
    private final RoleJPARepository roleJPARepository;

    @Transactional
    @Override
    public Paging<Role> findAll(Pageable pageable) {
        Page<RoleEntity> roleEntityPage = roleJPARepository.findAllByDeletedAtNull(pageable);
        return Paging.<Role>builder()
                .items(roleEntityPage.map(roleDBMapper::from).getContent())
                .page(roleEntityPage.getNumber())
                .size(roleEntityPage.getSize())
                .total(roleEntityPage.getNumberOfElements())
                .totalPages(roleEntityPage.getTotalPages()).build();
    }
    @Transactional
    @Override
    public Optional<Role> findById(Long id) {
        Optional<RoleEntity> roleEntityOptional = roleJPARepository.findByIdAndDeletedAtNull(id);
        return roleEntityOptional.map(roleDBMapper::from);
    }
    @Override
    public Role save(Role role) {
        RoleEntity roleEntity = roleDBMapper.from(role);
//        roleJPARepository.save(roleEntity);
//        return roleDBMapper.from(roleEntity);
        return roleDBMapper.from(roleJPARepository.save(roleEntity));
    }

    @Override
    public boolean existsByNames(String name) {
        boolean roleName = roleJPARepository.existsByName(name);
        if (roleName) return true;
        return false;
    }
}
