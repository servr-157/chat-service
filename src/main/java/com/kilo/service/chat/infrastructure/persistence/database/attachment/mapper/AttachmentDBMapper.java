package com.kilo.service.chat.infrastructure.persistence.database.attachment.mapper;

import com.kilo.service.chat.domain.attachment.entity.Attachment;
import com.kilo.service.chat.infrastructure.persistence.database.attachment.entity.AttachmentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AttachmentDBMapper {
    @Mapping(target = "message.id",source = "message.id")
    @Mapping(target = "type",source = "type")
    Attachment from(AttachmentEntity attachmentEntity);

    @Mapping(target = "message",source = "message")
    @Mapping(target = "type",source = "type")
    AttachmentEntity to(Attachment attachment);
}
