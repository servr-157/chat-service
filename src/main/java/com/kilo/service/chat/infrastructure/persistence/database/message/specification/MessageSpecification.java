package com.kilo.service.chat.infrastructure.persistence.database.message.specification;

import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationEntity;
import com.kilo.service.chat.infrastructure.persistence.database.message.entity.MessageEntity;
import com.kilo.service.chat.infrastructure.persistence.database.user.entity.UserEntity;
import jakarta.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;

public class MessageSpecification {

    public static Specification<MessageEntity> filter(Long conversationId, String userId, String keyword) {

        Specification<MessageEntity> spec = isNotDeleted();

        if (keyword != null) {
            spec = spec.and(search(keyword));
        }

        if (conversationId != null) {
            spec = spec.and(filterByConversation(conversationId));
        }

        if (userId != null) {
            spec = spec.and(filterByUser(userId));
        }

        return spec;
    }

    public static Specification<MessageEntity> search(String keyword) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("content"), "%" + keyword + "%");
    }

    public static Specification<MessageEntity> filterByConversation(Long conversationId) {
        return (root, query, criteriaBuilder) -> {

            Join<MessageEntity, ConversationEntity> joinConversation = root.join("conversation");
            return criteriaBuilder.equal(joinConversation.get("id"), conversationId);
        };
    }

    public static Specification<MessageEntity> filterByUser(String userId) {

        return (root, query, criteriaBuilder) -> {
            Join<MessageEntity, UserEntity> joinUser = root.join("user");
            return criteriaBuilder.equal(joinUser.get("userId"), userId);
        };
    }

    public static Specification<MessageEntity> isNotDeleted() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.isNull(root.get("deletedAt"));
    }
}
