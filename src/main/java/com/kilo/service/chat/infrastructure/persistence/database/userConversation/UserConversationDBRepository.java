package com.kilo.service.chat.infrastructure.persistence.database.userConversation;

import com.kilo.service.chat.domain.user.constant.UserConstant;
import com.kilo.service.chat.domain.user.exception.UserNotFountException;
import com.kilo.service.chat.domain.user.repository.UserRepository;
import com.kilo.service.chat.domain.userConversation.entity.UserConversation;
import com.kilo.service.chat.domain.userConversation.repository.UserConversationRepository;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.repository.ConversationEntityRepository;
import com.kilo.service.chat.infrastructure.persistence.database.role.repository.RoleJPARepository;
import com.kilo.service.chat.infrastructure.persistence.database.user.entity.UserEntity;
import com.kilo.service.chat.infrastructure.persistence.database.user.mapper.UserDBMapper;
import com.kilo.service.chat.infrastructure.persistence.database.userConversation.entity.UserConversationEntity;
import com.kilo.service.chat.infrastructure.persistence.database.userConversation.mapper.UserConversationDBMapper;
import com.kilo.service.chat.infrastructure.persistence.database.userConversation.repository.UserConversationJPARepository;
import com.kilo.service.chat.share.entity.domain.Paging;
import com.kilo.service.chat.share.utility.PageNumberUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserConversationDBRepository implements UserConversationRepository {
    private final UserRepository userRepository;
    private final UserDBMapper userDBMapper;
    private final UserConversationDBMapper mapper;
    private final UserConversationJPARepository userConversationJPARepository;
    private final RoleJPARepository roleJPARepository;
    private final ConversationEntityRepository conversationJPARepository;

    @Override
    public Paging<UserConversation> list(String userId,Long conversationId,Pageable pageable) {

        Page<UserConversationEntity> paging = userConversationJPARepository.findByUserIdAndConversationId(userId,conversationId,pageable);

        return Paging.<UserConversation>builder()
                .items(paging.map(mapper::from).getContent())
                .size(paging.getSize())
                .page(PageNumberUtility.out(paging.getNumber()))
                .total(paging.getNumberOfElements())
                .totalPages(paging.getTotalPages())
                .build();
    }

    @Override
    public Boolean existsUserInConversation(String userId, Long conversationId) {
        return userConversationJPARepository.existsByConversations_IdEqualsAndUser_UserIdContains(conversationId, userId);
    }

    @Override
    public Optional<UserConversation> findById(Long id) {

        Optional<UserConversationEntity> userConversationEntity = userConversationJPARepository.findById(id);

        return userConversationEntity.map(mapper::from);
    }

    @Override
    public UserConversation save(UserConversation userConversation) {
        UserConversationEntity userConversationEntity = mapper.to(userConversation);
        List<UserEntity> userEntities = userRepository.findByIds(userConversation.getUserId().stream().toList())
                .stream().map(userDBMapper::from).toList();
        for (UserEntity userEntity : userEntities) {
            Optional<UserEntity> optionalUser = userEntities.stream()
                    .filter(it -> it.getUserId().equals(userEntity.getUserId())).findFirst();

            if (optionalUser.isPresent()) {
                UserConversationEntity  userConversationEntity1 = new UserConversationEntity();
                userConversationEntity1.setUser(optionalUser.get());
                userConversationEntity1.setRole(roleJPARepository.getReferenceById(userConversation.getRoleId()));
                userConversationEntity1.setConversations(conversationJPARepository.getReferenceById(userConversation.getConversationId()));
                userConversationJPARepository.save(userConversationEntity1);
            }
            else
                throw new UserNotFountException(HttpStatus.NOT_FOUND,
                        String.format(UserConstant.USER_ID_NOT_FOUND,userConversation.getUserId()));
        }
        return mapper.from(userConversationEntity);
    }
}
