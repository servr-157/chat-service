package com.kilo.service.chat.infrastructure.persistence.database.column;

public enum AttachmentTypeEnum {
    IMAGE,
    FILE,
    VIDEO,
    AUDIO,
    ALL
}
