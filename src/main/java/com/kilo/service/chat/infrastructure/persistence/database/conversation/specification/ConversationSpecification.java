package com.kilo.service.chat.infrastructure.persistence.database.conversation.specification;

import com.kilo.service.chat.infrastructure.persistence.database.column.ConversationTypeEnum;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.entity.ConversationViewEntity;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;
import org.springframework.data.jpa.domain.Specification;

public class ConversationSpecification {

    public static Specification<ConversationViewEntity> filter(String keyword, String type, String userId) {

        Specification<ConversationViewEntity> spec = isNotDeleted();

        if (keyword != null) {
            spec = spec.and(search(keyword));
        }

        if (type != null) {
            spec = spec.and(type(type));
        }

        spec = spec.and(filterByUserId(userId));

        return spec;
    }

    public static Specification<ConversationViewEntity> search(String keyword) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("title"), "%" + keyword + "%");
    }

    public static Specification<ConversationViewEntity> type(String type) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("type"), ConversationTypeEnum.valueOf(type.toUpperCase()));
    }

    public static Specification<ConversationViewEntity> isNotDeleted() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.isNull(root.get("deletedAt"));
    }

    public static Specification<ConversationViewEntity> filterByUserId(String userId) {
        return (root, query, criteriaBuilder) -> {

//            Predicate privateConversation = criteriaBuilder.and(
//                    criteriaBuilder.equal(root.get("type"), "PRIVATE"),
//                    criteriaBuilder.notEqual(root.get("userId"), userId)
//            );
//
//            Predicate nonPrivateConversation = criteriaBuilder.and(
//                    criteriaBuilder.notEqual(root.get("type"), "PRIVATE"),
//                    criteriaBuilder.equal(root.get("userId"), userId)
//            );
//
//            return criteriaBuilder.or(privateConversation, nonPrivateConversation);
            Subquery<Long> subquery = query.subquery(Long.class);
            Root<ConversationViewEntity> subRoot = subquery.from(ConversationViewEntity.class);

            // Define the selection and where clause for the subquery
            subquery.select(subRoot.get("id"))
                    .distinct(true)
                    .where(criteriaBuilder.equal(subRoot.get("userId"), userId));

            // Main query where clause
            return criteriaBuilder.and(
                    criteriaBuilder.in(root.get("id")).value(subquery),
                    criteriaBuilder.notEqual(root.get("userId"), userId)
            );
        };
    }

    public static Specification<ConversationViewEntity> findConversationsById(String userId, Long conversationId) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.and(
                criteriaBuilder.equal(root.get("id"), conversationId),
                criteriaBuilder.notEqual(root.get("userId"), userId),
                criteriaBuilder.isNull(root.get("deletedAt"))
        );
    }
}
