package com.kilo.service.chat.infrastructure.persistence.database.role.mapper;

import com.kilo.service.chat.domain.role.entity.Role;
import com.kilo.service.chat.infrastructure.persistence.database.role.entity.RoleEntity;
import org.mapstruct.Mapper;

@Mapper
public interface RoleDBMapper {
    Role from(RoleEntity roleEntity);
    RoleEntity from(Role role);
}
