package com.kilo.service.chat.infrastructure.persistence.database.message.repository;

import com.kilo.service.chat.infrastructure.persistence.database.message.entity.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MessageJPARepository extends JpaRepository<MessageEntity, Long>, JpaSpecificationExecutor<MessageEntity> {

    Optional<MessageEntity> findByIdAndDeletedAtNull(Long id);

    @Query("""
            select (count(m) > 0) from MessageEntity m
            where m.user.userId = :userId and m.id = :id and m.deletedAt is null""")
    boolean existsByUser_UserIdEqualsAndIdEqualsAndDeletedAtNull(@Param("userId") String userId, @Param("id") Long id);


}