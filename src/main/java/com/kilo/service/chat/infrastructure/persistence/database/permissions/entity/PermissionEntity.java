package com.kilo.service.chat.infrastructure.persistence.database.permissions.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kilo.service.chat.infrastructure.persistence.database.role.entity.RoleEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "permissions")
@Getter
@Setter
public class PermissionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "module", nullable = false)
    private String module;

    @JsonIgnore
    @ManyToMany(mappedBy = "permissionEntities")
    private Set<RoleEntity> roleEntities;
}
