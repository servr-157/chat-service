package com.kilo.service.chat.infrastructure.persistence.database.message;

import com.kilo.service.chat.domain.message.entity.Message;
import com.kilo.service.chat.domain.message.repository.MessageRepository;
import com.kilo.service.chat.infrastructure.persistence.database.conversation.repository.ConversationEntityRepository;
import com.kilo.service.chat.infrastructure.persistence.database.message.entity.MessageEntity;
import com.kilo.service.chat.infrastructure.persistence.database.message.mapper.MessageDBMapper;
import com.kilo.service.chat.infrastructure.persistence.database.message.repository.MessageJPARepository;
import com.kilo.service.chat.infrastructure.persistence.database.message.specification.MessageSpecification;
import com.kilo.service.chat.infrastructure.persistence.database.user.repository.UserEntityRepository;
import com.kilo.service.chat.share.entity.domain.Paging;
import com.kilo.service.chat.share.utility.PageNumberUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MessageDBRepository implements MessageRepository {

    private final MessageJPARepository messageJPARepository;
    private final UserEntityRepository userEntityRepository;
    private final ConversationEntityRepository conversationEntityRepository;
    private final MessageDBMapper mapper;

    @Transactional
    @Override
    public Paging<Message> findAll(int page, int size, Long conversationId, String userId, String keyword) {

        Page<MessageEntity> entityPage = messageJPARepository.findAll(MessageSpecification.filter(conversationId, userId, keyword),
                PageRequest.of(PageNumberUtility.in(page), size, Sort.Direction.DESC, "id"));

        List<Message> messages = entityPage.stream().map(mapper::from).toList();

        return Paging.<Message>builder()
                .items(messages)
                .page(PageNumberUtility.out(entityPage.getNumber()))
                .size(entityPage.getSize())
                .total(entityPage.getNumberOfElements())
                .totalPages(entityPage.getTotalPages())
                .build();
    }

    @Transactional
    @Override
    public Message save(Message message) {

        MessageEntity messageEntity = mapper.from(message);
        messageEntity.setUser(userEntityRepository.getReferenceById(message.getUser().getUserId()));
        messageEntity.setConversation(conversationEntityRepository.getReferenceById(message.getConversation().getId()));
        return mapper.from(messageJPARepository.save(messageEntity));
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<Message> findById(Long id) {

        Optional<MessageEntity> messageEntity = messageJPARepository.findByIdAndDeletedAtNull(id);
        return messageEntity.map(mapper::from);
    }

    @Override
    public Boolean isOwnMessage(String userId, Long messageId) {
        return messageJPARepository.existsByUser_UserIdEqualsAndIdEqualsAndDeletedAtNull(userId, messageId);
    }

}
