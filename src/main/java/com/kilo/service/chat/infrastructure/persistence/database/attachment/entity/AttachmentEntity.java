package com.kilo.service.chat.infrastructure.persistence.database.attachment.entity;

import com.kilo.service.chat.infrastructure.persistence.database.column.AttachmentTypeEnum;
import com.kilo.service.chat.infrastructure.persistence.database.message.entity.MessageEntity;
import com.kilo.service.chat.share.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "attachments")
@Getter
@Setter
public class AttachmentEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "message_id", nullable = false)
    private MessageEntity message;

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @Column(name = "file_path", nullable = false)
    private String filePath;

    @Column(name = "compression_path", nullable = false)
    private String compressionPath;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private AttachmentTypeEnum type;
}
