package com.kilo.service.chat.infrastructure.persistence.database.role.repository;

import com.kilo.service.chat.infrastructure.persistence.database.role.entity.RoleEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface RoleJPARepository extends JpaRepository<RoleEntity, Long> {

    Page<RoleEntity> findAllByDeletedAtNull(Pageable pageable);
    @Query("select r from RoleEntity r where r.id = :id and r.deletedAt is null")
    Optional<RoleEntity> findByIdAndDeletedAtNull(@Param("id") Long id);

    @Query("select (count(r) > 0) from RoleEntity r where r.name = :name")
    boolean existsByName(@Param("name") String name);


}
