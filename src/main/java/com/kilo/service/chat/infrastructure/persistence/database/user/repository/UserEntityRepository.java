package com.kilo.service.chat.infrastructure.persistence.database.user.repository;

import com.kilo.service.chat.infrastructure.persistence.database.user.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserEntityRepository extends JpaRepository<UserEntity, String> {

    Optional<UserEntity> findByUserIdAndDeletedAtNull(String userId);
}