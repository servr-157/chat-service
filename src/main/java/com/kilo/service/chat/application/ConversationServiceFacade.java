package com.kilo.service.chat.application;

import com.kilo.service.chat.domain.conversation.constant.ConversationConstant;
import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.conversation.entity.ConversationView;
import com.kilo.service.chat.domain.conversation.exception.ConversationNotFountException;
import com.kilo.service.chat.domain.conversation.repository.ConversationRepository;
import com.kilo.service.chat.domain.conversation.service.ConversationService;
import com.kilo.service.chat.domain.userConversation.entity.UserConversation;
import com.kilo.service.chat.domain.userConversation.repository.UserConversationRepository;
import com.kilo.service.chat.share.entity.domain.HeaderCommand;
import com.kilo.service.chat.share.entity.domain.Paging;
import com.kilo.service.chat.share.utility.RequestHeaderUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Collections;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConversationServiceFacade implements ConversationService {

    private final ConversationRepository conversationRepository;
    private final UserConversationRepository userConversationRepository;

    @Override
    public Paging<ConversationView> findAll(int page, int size, String keyword, String type) {
        return conversationRepository.findAll(page, size, keyword, type, RequestHeaderUtility.getHeaderCommand().getUserID());
    }

    @Override
    public Conversation save(Conversation conversation) {

        HeaderCommand headerCommand = RequestHeaderUtility.getHeaderCommand();
        conversation.setCreatedBy(headerCommand.getUserID());
        Conversation saveConversation = conversationRepository.save(conversation);

        // add user to group
        if (conversation.getType().equals("GROUP")) {
            UserConversation userConversation = new UserConversation();
            userConversation.setConversationId(saveConversation.getId());
            userConversation.setUserId(Collections.singletonList(headerCommand.getUserID()));
            userConversation.setRoleId(1L);
            userConversationRepository.save(userConversation);
        }
        return saveConversation;
    }

    @Override
    public Conversation update(Long id, Conversation conversation) {

        HeaderCommand headerCommand = RequestHeaderUtility.getHeaderCommand();
        Optional<Conversation> conversationOptional = conversationRepository.findById(id);

        if (conversationOptional.isEmpty())
            throw new ConversationNotFountException(String.format(ConversationConstant.CONVERSATION_ID_NOT_FOUND, id));

        Conversation conversationOld = conversationOptional.get();
        BeanUtils.copyProperties(conversation, conversationOld, "id", "createdBy", "createdAt");
        conversationOld.setUpdatedAt(Instant.now());
        conversationOld.setUpdatedBy(headerCommand.getUserID());

        return conversationRepository.save(conversationOld);
    }

    @Override
    public Conversation delete(Long id) {
        Optional<Conversation> conversationOptional = conversationRepository.findById(id);

        if (conversationOptional.isEmpty())
            throw new ConversationNotFountException(HttpStatus.NOT_FOUND, String.format(ConversationConstant.CONVERSATION_ID_NOT_FOUND, id));

        Conversation conversationOld = conversationOptional.get();
        conversationOld.setDeletedAt(Instant.now());

        return conversationRepository.save(conversationOld);
    }

    @Override
    public ConversationView findById(Long id) {
        return conversationRepository.findByConversationId(RequestHeaderUtility.getHeaderCommand().getUserID(), id).
                orElseThrow(() -> new ConversationNotFountException(String.format(ConversationConstant.CONVERSATION_ID_NOT_FOUND,id)));
    }
}
