package com.kilo.service.chat.application;

import com.kilo.service.chat.domain.role.constant.RoleConstant;
import com.kilo.service.chat.domain.role.entity.Role;
import com.kilo.service.chat.domain.role.exception.RoleException;
import com.kilo.service.chat.domain.role.exception.RoleNotFoundException;
import com.kilo.service.chat.domain.role.repository.RoleRepository;
import com.kilo.service.chat.domain.role.service.RoleService;
import com.kilo.service.chat.share.entity.domain.Paging;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.BadRequestException;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class RoleServiceFacade implements RoleService {

    private final RoleRepository roleRepository;
    @Override
    public Paging<Role> findAllRoles(Pageable pageable) {
        return roleRepository.findAll(pageable);
    }
    @Transactional
    @Override
    public Role findById(Long id) {
        Optional<Role> role = roleRepository.findById(id);
        if (role.isEmpty()) {
            throw new RoleNotFoundException(String.format(RoleConstant.ROLE_NOT_FOUND, id),HttpStatus.NOT_FOUND);
//            throw new RoleNotFoundException();
        }
        return role.get();
    }

    @Override
    public Role create(Role role) {
        boolean roleName = roleRepository.existsByNames(role.getName());
        if (roleName){
            throw new RoleNotFoundException(String.format(RoleConstant.EXIST_ROLE_NAME,role.getName()),HttpStatus.BAD_REQUEST);
        }
        role.setCreatedAt(Instant.now());
        return roleRepository.save(role);
    }

    @Override
    public Role edite(Role role, Long id) {
        Optional<Role> roleOptional = roleRepository.findById(id);
        if (roleOptional.isEmpty()) {
            throw new RoleNotFoundException(String.format(RoleConstant.ROLE_NOT_FOUND, id),HttpStatus.NOT_FOUND);
        }
        Role editedRole = roleOptional.get();
        BeanUtils.copyProperties(role, editedRole,"id","createdAt");
        editedRole.setCreatedAt(Instant.now());
        return roleRepository.save(editedRole);
    }
    @Transactional
    @Override
    public Role delete(Long id) {
        Optional<Role> roleOptional = roleRepository.findById(id);
        if (roleOptional.isEmpty()) {
            throw new RoleNotFoundException(String.format(RoleConstant.ROLE_NOT_FOUND, id),HttpStatus.NOT_FOUND);
        }
        Role role = roleOptional.get();
        role.setDeletedAt(Instant.now());
        return roleRepository.save(role);
    }
}
