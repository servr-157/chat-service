package com.kilo.service.chat.application;

import com.kilo.service.chat.domain.conversation.constant.ConversationConstant;
import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.conversation.exception.ConversationNotFountException;
import com.kilo.service.chat.domain.conversation.repository.ConversationRepository;
import com.kilo.service.chat.domain.user.constant.UserConstant;
import com.kilo.service.chat.domain.user.exception.UserNotFountException;
import com.kilo.service.chat.domain.user.repository.UserRepository;
import com.kilo.service.chat.domain.userConversation.constant.UserConversationConstant;
import com.kilo.service.chat.domain.userConversation.entity.UserConversation;
import com.kilo.service.chat.domain.userConversation.exception.UserConversationNotFoundException;
import com.kilo.service.chat.domain.userConversation.repository.UserConversationRepository;
import com.kilo.service.chat.domain.userConversation.service.UserConversationService;
import com.kilo.service.chat.share.entity.domain.Paging;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserConversationServiceFacade implements UserConversationService {

    private final UserConversationRepository userConversationRepository;
    private final ConversationRepository conversationRepository;
    private final UserRepository userRepository;

    @Transactional(readOnly = true)
    @Override
    public Paging<UserConversation> list(String userId,Long conversationId, Pageable pageable) {
        return userConversationRepository.list(userId,conversationId,pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public UserConversation findById(Long id) {
        return userConversationRepository.findById(id).orElseThrow(
                ()-> new UserConversationNotFoundException(HttpStatus.NOT_FOUND,
                        String.format(UserConversationConstant.ID_NOT_FOUND,id)));
    }

    @Transactional
    @Override
    public UserConversation save(UserConversation userConversation) {
        Optional<Conversation> conversation = conversationRepository.findById(userConversation.getConversationId());


        if (userRepository.findById(userConversation.getUserId().stream().findFirst().get()).isEmpty())
            throw new UserNotFountException(HttpStatus.NOT_FOUND,
                    String.format(UserConstant.USER_ID_NOT_FOUND,userConversation.getUserId()));

        if (conversation.isEmpty())
            throw new ConversationNotFountException(HttpStatus.NOT_FOUND,
                    String.format(ConversationConstant.CONVERSATION_ID_NOT_FOUND
                            , userConversation.getConversationId()));

        if(!userConversationRepository.existsUserInConversation(userConversation.getUserId().stream().findFirst().get(),
                userConversation.getConversationId()))
            return userConversationRepository.save(userConversation);
        return null;
    }

    @Override
    public void deleteById(Long id) {
        Optional<UserConversation> userConversation = userConversationRepository.findById(id);
        if(userConversation.isEmpty())
            throw new UserConversationNotFoundException(HttpStatus.NOT_FOUND,
                        String.format(UserConversationConstant.ID_NOT_FOUND,id));
        UserConversation conversationToDelete = userConversation.get();
        conversationToDelete.setDeletedAt(Instant.now());
        userConversationRepository.save(conversationToDelete);
    }
}
