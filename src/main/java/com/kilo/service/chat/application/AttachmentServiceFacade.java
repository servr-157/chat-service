package com.kilo.service.chat.application;

import com.kilo.service.chat.domain.attachment.constant.AttachmentConstant;
import com.kilo.service.chat.domain.attachment.entity.Attachment;
import com.kilo.service.chat.domain.attachment.exception.AttachmentNotFoundException;
import com.kilo.service.chat.domain.attachment.repository.AttachmentRepository;
import com.kilo.service.chat.domain.attachment.service.AttachmentService;
import com.kilo.service.chat.domain.message.exception.MessageNotFountException;
import com.kilo.service.chat.domain.message.repository.MessageRepository;
import com.kilo.service.chat.share.constant.HttpMessageConstant;
import com.kilo.service.chat.share.entity.domain.Paging;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AttachmentServiceFacade implements AttachmentService {
    private final AttachmentRepository attachmentRepository;
    private final MessageRepository messageRepository;
    @Transactional(readOnly = true)
    @Override
    public Paging<Attachment> filter(String fileName, String type, Pageable pageable) {
        return attachmentRepository.filter(fileName, type, pageable);
    }
    @Override
    public Attachment findById(Long id) {
        return attachmentRepository.findById(id).orElseThrow(
                ()-> new AttachmentNotFoundException(HttpStatus.NOT_FOUND,
                        String.format(AttachmentConstant.ID_NOT_FOUND,id)));
    }

    @Override
    public Attachment save(Attachment attachment) {
//        HeaderCommand headerCommand = RequestHeaderUtility.getHeaderCommand();
        if(messageRepository.findById(attachment.getMessage().getId()).isEmpty())
            throw new MessageNotFountException(HttpStatus.NOT_FOUND, HttpMessageConstant.ERROR);
        return attachmentRepository.save(attachment);
    }

    @Override
    public void deleteById(Long id) {
//        HeaderCommand headerCommand = RequestHeaderUtility.getHeaderCommand();
        Optional<Attachment> attachmentOptional = attachmentRepository.findById(id);
        if (attachmentOptional.isEmpty())
            throw new AttachmentNotFoundException(HttpStatus.NOT_FOUND,
                    String.format(AttachmentConstant.ID_NOT_FOUND,id));
        Attachment AttachmentToDelete= attachmentOptional.get();
        AttachmentToDelete.setDeletedAt(Instant.now());
        attachmentRepository.save(AttachmentToDelete);
    }
}
