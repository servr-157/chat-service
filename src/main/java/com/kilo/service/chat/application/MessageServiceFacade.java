package com.kilo.service.chat.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kilo.service.chat.domain.conversation.constant.ConversationConstant;
import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.conversation.exception.ConversationNotFountException;
import com.kilo.service.chat.domain.conversation.repository.ConversationRepository;
import com.kilo.service.chat.domain.message.constant.MessageConstant;
import com.kilo.service.chat.domain.message.entity.Message;
import com.kilo.service.chat.domain.message.exception.MessageInvalidException;
import com.kilo.service.chat.domain.message.exception.MessageNotFountException;
import com.kilo.service.chat.domain.message.repository.MessageRepository;
import com.kilo.service.chat.domain.message.service.MessageService;
import com.kilo.service.chat.domain.user.constant.UserConstant;
import com.kilo.service.chat.domain.user.entity.User;
import com.kilo.service.chat.domain.user.exception.UserNotFountException;
import com.kilo.service.chat.domain.user.repository.UserRepository;
import com.kilo.service.chat.presentation.message.mapper.MessageMapper;
import com.kilo.service.chat.share.entity.domain.HeaderCommand;
import com.kilo.service.chat.share.entity.domain.Paging;
import com.kilo.service.chat.share.utility.RequestHeaderUtility;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
@Slf4j
public class MessageServiceFacade implements MessageService {
    private final MessageRepository messageRepository;
    private final ConversationRepository conversationRepository;
    private final UserRepository userRepository;
    private final ObjectMapper objectMapper;
    private final MessageMapper messageMapper;

    @Override
    public Paging<Message> findAll(int page, int size, Long conversationId, String userId, String keyword) {
        return messageRepository.findAll(page, size, conversationId, userId, keyword);
    }

    @Override
    public Message save(Message message) {

        HeaderCommand headerCommand = RequestHeaderUtility.getHeaderCommand();
        Optional<Conversation> conversation = conversationRepository.findById(message.getConversation().getId());
        Optional<User> user = userRepository.findById(headerCommand.getUserID());

        if (user.isEmpty())
            throw new UserNotFountException(format(UserConstant.USER_ID_NOT_FOUND, headerCommand.getUserID()));

        if (conversation.isEmpty())
            throw new ConversationNotFountException(HttpStatus.NOT_FOUND, format(ConversationConstant.CONVERSATION_ID_NOT_FOUND, message.getConversation().getId()));

        message.getUser().setUserId(headerCommand.getUserID());
        Message saved = messageRepository.save(message);

        try {

            String cm = objectMapper.writeValueAsString(messageMapper.from(saved));

            Conversation conversationEntity = conversation.get();
            conversationEntity.setId(message.getConversation().getId());
            conversationEntity.setLastMessage(cm);
            conversationRepository.save(conversationEntity);

        }catch (JsonProcessingException e) {
            throw new MessageInvalidException( e.getMessage());
        }

        return saved;
    }

    @Override
    public Message update(Long id, Message message) {
        HeaderCommand headerCommand = RequestHeaderUtility.getHeaderCommand();
        Optional<Message> messageOptional = messageRepository.findById(id);

        userRepository.findById(headerCommand.getUserID()).orElseThrow(() -> new UserNotFountException(format(UserConstant.USER_ID_NOT_FOUND, headerCommand.getUserID())));

        if (messageOptional.isEmpty())
            throw new MessageNotFountException(String.format(MessageConstant.MESSAGE_ID_NOT_FOUND, id));

        if (!messageRepository.isOwnMessage(headerCommand.getUserID(), id))
            throw new MessageInvalidException(String.format(MessageConstant.MESSAGE_INVALID, id));


        Message messageToUpdate = messageOptional.get();
        BeanUtils.copyProperties(message, messageToUpdate, "id", "createdAt", "createdBy", "userId");
        messageToUpdate.setUpdatedAt(Instant.now());

        return messageRepository.save(messageToUpdate);
    }

    @Override
    public Message delete(Long id) {

        HeaderCommand headerCommand = RequestHeaderUtility.getHeaderCommand();
        Optional<Message> message = messageRepository.findById(id);

        userRepository.findById(headerCommand.getUserID()).orElseThrow(() -> new UserNotFountException(format(UserConstant.USER_ID_NOT_FOUND, headerCommand.getUserID())));

        if (message.isEmpty())
            throw new MessageNotFountException(String.format(MessageConstant.MESSAGE_ID_NOT_FOUND, id));

        if (!messageRepository.isOwnMessage(headerCommand.getUserID(), id))
            throw new MessageInvalidException(String.format(MessageConstant.MESSAGE_INVALID, id));

        Message messageToDelete = message.get();
        messageToDelete.setDeletedAt(Instant.now());

        return messageRepository.save(messageToDelete);
    }

}
