package com.kilo.service.chat.share.utility;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AspectUtility {

  /**
   * get method annotation
   *
   * @param joinPoint joint point
   * @param annotationClass annotation
   * @param <A> clazz
   * @return annotation
   */
  public static <A extends Annotation> A getMethodAnnotation(
      JoinPoint joinPoint, Class<A> annotationClass) {
    final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    return signature.getMethod().getAnnotation(annotationClass);
  }

  /**
   * get method annotation
   *
   * @param joinPoint joint point
   * @param annotationClass annotation
   * @param <A> clazz
   * @return annotation
   */
  public static <A extends Annotation> A getMethodAnnotation(
      ProceedingJoinPoint joinPoint, Class<A> annotationClass) {
    final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    return signature.getMethod().getAnnotation(annotationClass);
  }

  /**
   * get value from annotation field
   *
   * @param joinPoint joint point
   * @param annotationClass annotation
   * @param <R> field class
   * @param <A> annotation class
   * @return field value
   */
  @SuppressWarnings("unchecked")
  public static <R, A extends Annotation> R getValueFieldAnnotation(
      JoinPoint joinPoint, Class<A> annotationClass) {
    final Object[] args = joinPoint.getArgs();

    final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    final Parameter[] parameters = signature.getMethod().getParameters();

    for (int i = 0; i < parameters.length; i++) {

      final Parameter parameter = parameters[i];
      final A annotation = parameter.getAnnotation(annotationClass);

      if (Objects.isNull(annotation) && Objects.isNull(args[i])) continue;

      final Class<?> type = parameter.getType();

      return Arrays.stream(args)
          .filter(it -> type.isAssignableFrom(it.getClass()))
          .findFirst()
          .map(
              arg -> {
                final Field[] fields = type.getDeclaredFields();
                for (Field field : fields) {
                  field.setAccessible(true);
                  final A fieldAnnotation = field.getAnnotation(annotationClass);
                  if (Objects.nonNull(fieldAnnotation)) {
                    try {
                      return (R) field.get(arg);
                    } catch (IllegalAccessException e) {
                      return null;
                    }
                  }
                }
                return null;
              })
          .orElse(null);
    }

    return null;
  }
}
