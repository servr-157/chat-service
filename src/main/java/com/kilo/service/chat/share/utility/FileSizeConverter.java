package com.kilo.service.chat.share.utility;

/**
 * Utility class for converting file sizes between different units.
 */
public class FileSizeConverter {

    private static final long KILOBYTE = 1024;
    private static final long MEGABYTE = KILOBYTE * 1024;
    private static final long GIGABYTE = MEGABYTE * 1024;
    private static final long TERABYTE = GIGABYTE * 1024;

    /**
     * Convert bytes to kilobytes.
     *
     * @param bytes The size in bytes.
     * @return The size in kilobytes.
     */
    public static double bytesToKilobytes(long bytes) {
        return (double) bytes / KILOBYTE;
    }

    /**
     * Convert bytes to megabytes.
     *
     * @param bytes The size in bytes.
     * @return The size in megabytes.
     */
    public static double bytesToMegabytes(long bytes) {
        return (double) bytes / MEGABYTE;
    }

    /**
     * Convert bytes to gigabytes.
     *
     * @param bytes The size in bytes.
     * @return The size in gigabytes.
     */
    public static double bytesToGigabytes(long bytes) {
        return (double) bytes / GIGABYTE;
    }

    /**
     * Convert bytes to terabytes.
     *
     * @param bytes The size in bytes.
     * @return The size in terabytes.
     */
    public static double bytesToTerabytes(long bytes) {
        return (double) bytes / TERABYTE;
    }

    /**
     * Convert kilobytes to bytes.
     *
     * @param kilobytes The size in kilobytes.
     * @return The size in bytes.
     */
    public static long kilobytesToBytes(double kilobytes) {
        return (long) (kilobytes * KILOBYTE);
    }

    /**
     * Convert megabytes to bytes.
     *
     * @param megabytes The size in megabytes.
     * @return The size in bytes.
     */
    public static long megabytesToBytes(double megabytes) {
        return (long) (megabytes * MEGABYTE);
    }

    /**
     * Convert gigabytes to bytes.
     *
     * @param gigabytes The size in gigabytes.
     * @return The size in bytes.
     */
    public static long gigabytesToBytes(double gigabytes) {
        return (long) (gigabytes * GIGABYTE);
    }

    /**
     * Convert terabytes to bytes.
     *
     * @param terabytes The size in terabytes.
     * @return The size in bytes.
     */
    public static long terabytesToBytes(double terabytes) {
        return (long) (terabytes * TERABYTE);
    }
}
