package com.kilo.service.chat.share.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HttpResponseEntity<T> {

  // The status of the HTTP response
  @Builder.Default
  private Integer status = HttpStatus.OK.value();

  // The message of the HTTP response
  @JsonInclude
  @Builder.Default
  private String message = "Successful";

  // The data of the HTTP response
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private T data;

  // The pagination information of the HTTP response
  private PagingEntity paging;

  // The error information of the HTTP response
  private ErrorEntity error;

  // Constructor for a single data object
  public HttpResponseEntity(HttpStatus status, String message, T data) {
    this.status = status.value();
    this.message = message;
    this.data = data;
  }

  // Constructor for an error object
  public HttpResponseEntity(HttpStatus status, String message, ErrorEntity error) {
    this.status = status.value();
    this.message = message;
    this.error = error;
  }

  // Constructor for a list of data objects
  public HttpResponseEntity(HttpStatus status, String message, List<T> data) {
    this.status = status.value();
    this.message = message;
    this.data = (T) data;
  }

  // Constructor for a list of data objects with pagination information
  public HttpResponseEntity(HttpStatus status, String message, List<T> data, Object paging) {
    this.status = status.value();
    this.message = message;
    this.data = (T) data;
    this.paging =
        paging instanceof Map
            ? PagingEntity.from((Map<String, Object>) paging)
            : PagingEntity.from(parameters(paging));
  }

  // Setter for the status of the HTTP response
  public void setStatus(HttpStatus status) {
    this.status = status.value();
  }

  // Method to convert an object into a map
  public Map<String, Object> parameters(Object obj) {
    if (obj == null) {
      return null;
    }
    Map<String, Object> map = new HashMap<>();
    for (Field field : obj.getClass().getDeclaredFields()) {
      field.setAccessible(true);
      try {
        map.put(field.getName(), field.get(obj));
      } catch (Exception e) {
      }
    }
    return map;
  }
}
