package com.kilo.service.chat.share.annotion;

import org.springframework.http.HttpStatus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MockHttp {

  String file();

  Class<?> value();

  HttpStatus status() default HttpStatus.OK;

  boolean collection() default false;

  boolean pagination() default false;
}
