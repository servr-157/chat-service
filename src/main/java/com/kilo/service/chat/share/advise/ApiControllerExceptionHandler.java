package com.kilo.service.chat.share.advise;

import com.kilo.service.chat.share.constant.DomainErrorConstants;
import com.kilo.service.chat.share.entity.ErrorEntity;
import com.kilo.service.chat.share.entity.HttpResponseEntity;
import com.kilo.service.chat.share.exception.DomainException;
import com.kilo.service.chat.share.exception.HeaderNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestControllerAdvice
@RequiredArgsConstructor
public class ApiControllerExceptionHandler {

    private final Environment env;

    @ExceptionHandler(value = DomainException.class)
    public ResponseEntity<Object> handleDomainException(DomainException exception) {
        DomainErrorConstants errorConstants = Optional.ofNullable(exception.getError()).orElse(DomainErrorConstants.UNKNOWN_ERROR);
        String exceptionMessage = Optional.ofNullable(exception.getMessage()).orElse("An unexpected error occurred");

        ErrorEntity errorEntity = createErrorEntity(exception, errorConstants);

        HttpResponseEntity<Object> response = HttpResponseEntity.builder()
                .status(exception.getHttpStatus().value())
                .message(exceptionMessage)
                .error(errorEntity)
                .build();

        return ResponseEntity.status(exception.getHttpStatus()).body(response);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleValidationExceptions(MethodArgumentNotValidException ex) {
        List<String> validationErrors = ex.getBindingResult().getAllErrors().stream()
                .map(this::getValidationError)
                .toList();

        ErrorEntity errorEntity = createErrorEntity(ex, DomainErrorConstants.VALIDATION_ERROR, validationErrors);

        HttpResponseEntity<Object> response = HttpResponseEntity.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("Validation error")
                .error(errorEntity)
                .build();

        return ResponseEntity.badRequest().body(response);
    }

    private ErrorEntity createErrorEntity(Exception exception, DomainErrorConstants errorConstants) {
        return createErrorEntity(exception, errorConstants, Collections.emptyList());
    }

    private ErrorEntity createErrorEntity(Exception exception, DomainErrorConstants errorConstants, List<String> errors) {
        StackTraceElement[] stackTraceElements = Optional.ofNullable(exception.getCause())
                .map(Throwable::getStackTrace)
                .orElse(exception.getStackTrace());

        var message = errors.stream().findFirst().orElse(errorConstants.getMessage());

        return ErrorEntity.builder()
                .type(exception.getClass().getSimpleName())
                .code(errorConstants.name())
                .message(message)
                .error(Boolean.parseBoolean(env.getProperty("exception.stack-trace-elements.enabled")) ? Arrays.toString(stackTraceElements) : null)
                .build();
    }

    private String getValidationError(ObjectError objectError) {
        if (objectError instanceof FieldError fieldError) {
            return fieldError.getField() + ": " + fieldError.getDefaultMessage();
        }
        return objectError.getDefaultMessage();
    }

    @ExceptionHandler(HeaderNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> headerNotFoundException(HeaderNotFoundException exception) {
        return this.getBadRequestErrorElement(exception.getStackTrace(), exception.getClass().getName(), exception.getMessage(), exception);
    }


    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> missingServletRequestParameterException(MissingServletRequestParameterException exception) {
        return this.getBadRequestErrorElement(exception.getStackTrace(), exception.getClass().getName(), exception.getMessage(), exception);
    }

    protected ResponseEntity<Object> getBadRequestErrorElement(StackTraceElement[] stackTrace, String name, String message, Exception exception) {
        ErrorEntity errorEntity = new ErrorEntity();
        errorEntity.setCode(HttpStatus.BAD_REQUEST.toString());

        boolean showMessage = env.getProperty("exception.stack-trace-elements.enabled") != null ? Boolean.parseBoolean(env.getProperty("exception.stack-trace-elements.enabled")) : Boolean.TRUE;
        if (showMessage) {
            errorEntity.setMessage(stackTrace[0].toString());
            errorEntity.setType(name);
        }

        HttpResponseEntity<Object> response = HttpResponseEntity.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message(message)
                .error(errorEntity)
                .build();

        return ResponseEntity.badRequest().body(response);
    }

}
