package com.kilo.service.chat.share.exception.role;

import com.kilo.service.chat.share.exception.DomainException;

public class RoleNotFoundException extends DomainException {
    public RoleNotFoundException(String message) {
        super(message);
    }
}
