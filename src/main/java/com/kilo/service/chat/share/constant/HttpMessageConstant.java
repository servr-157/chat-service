package com.kilo.service.chat.share.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HttpMessageConstant {
  public static final String SUCCESS = "SUCCESS";
  public static final String ERROR = "ERROR";
}
