package com.kilo.service.chat.share.entity.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Builder
public class HeaderCommand {
    private String appID;
    private Long merchantID;
    private String userID;
}
