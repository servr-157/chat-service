package com.kilo.service.chat.share.exception;


import com.kilo.service.chat.share.constant.DomainErrorConstants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@Data
public class DomainException extends RuntimeException {

    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    private DomainErrorConstants error;
    private Boolean translation;

    public DomainException() {
        super();
    }

    public DomainException(String message) {
        super(message);
    }

    public DomainException(Throwable throwable) {
        super(throwable);
    }

    public DomainException(String message, Boolean translation) {
        super(message);
        this.translation = translation;
    }


    public DomainException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public DomainException(String message, Boolean translation, Throwable throwable) {
        super(message, throwable);
        this.translation = translation;
    }

    public DomainException(DomainErrorConstants error, String message) {
        super(message);
        this.error = error;
        this.translation = Boolean.FALSE;
    }



    public DomainException(DomainErrorConstants error) {
        super();
        this.error = error;
        this.translation = Boolean.FALSE;

    }

    public DomainException(DomainErrorConstants error, Throwable throwable) {
        super(throwable);
        this.error = error;
        this.translation = Boolean.FALSE;
    }

    public DomainException(DomainErrorConstants error, Boolean translation) {
        super();
        this.error = error;
        this.translation = translation;
    }

    public DomainException(DomainErrorConstants error, Boolean translation, Throwable throwable) {
        super(throwable);
        this.error = error;
        this.translation = translation;
    }

    public DomainException(DomainErrorConstants error, String message, Boolean translation) {
        super(message);
        this.error = error;
        this.translation = translation;
    }

    public DomainException(DomainErrorConstants error, String message, Boolean translation, Throwable throwable) {
        super(message, throwable);
        this.error = error;
        this.translation = translation;
    }

    public DomainException(HttpStatus status, String message) {
        super(message);
        this.httpStatus = status;
    }

}
