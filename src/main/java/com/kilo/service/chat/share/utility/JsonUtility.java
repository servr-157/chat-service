package com.kilo.service.chat.share.utility;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.io.File;
import java.util.List;
import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JsonUtility {

  private static final ObjectMapper mapper;

  static {
    mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
  }

  @SneakyThrows
  public static <T> String toString(T value) {
    return mapper.writeValueAsString(value);
  }

  @SneakyThrows
  public static <T> String toPrettyString(T value) {
    return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(value);
  }

  @SneakyThrows
  public static <T> T parse(String content, Class<T> clazz) {
    return mapper.readValue(content, clazz);
  }

  @SneakyThrows
  public static <T> List<T> parseAsList(String content, Class<T> clazz) {
    JavaType type = mapper.getTypeFactory().constructParametricType(List.class, clazz);
    return mapper.readValue(content, type);
  }

  @SneakyThrows
  public static <T> T parse(File file, Class<T> clazz) {
    return mapper.readValue(file, clazz);
  }

  @SneakyThrows
  public static <T> List<T> parseAsList(File file, Class<T> clazz) {
    JavaType type = mapper.getTypeFactory().constructParametricType(List.class, clazz);
    return mapper.readValue(file, type);
  }


  @SneakyThrows
  public static <T, K, V> Map<K, V> convert(T obj, Class<K> keyType, Class<V> valueType) {
    return mapper.convertValue(obj, mapper.getTypeFactory().constructMapType(Map.class, keyType, valueType));
  }
}
