package com.kilo.service.chat.share.aspect;

import com.kilo.service.chat.share.annotion.MockHttp;
import com.kilo.service.chat.share.constant.HttpMessageConstant;
import com.kilo.service.chat.share.entity.HttpResponseEntity;
import com.kilo.service.chat.share.entity.PagingEntity;
import com.kilo.service.chat.share.utility.AspectUtility;
import com.kilo.service.chat.share.utility.FileUtility;
import com.kilo.service.chat.share.utility.JsonUtility;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Aspect
@Component
public class MockHttpAspect {

  // Define a pointcut for methods annotated with @MockHttp
  @Pointcut(value = "@annotation(com.kiloit.kilotaskservice.share.annotion.MockHttp)")
  public void mockHttpPointCut() {}

  // Aspect around advice for methods annotated with @MockHttp
  @SneakyThrows
  @Around(value = "mockHttpPointCut()")
  public Object mock(ProceedingJoinPoint joinPoint) {

    // Get the @MockHttp annotation
    final MockHttp annotation = AspectUtility.getMethodAnnotation(joinPoint, MockHttp.class);

    // Get the class to parse the file content into
    final Class<?> clazz = annotation.value();

    // Get the file content and parse it into an object or a list of objects
    final Object data =
        FileUtility.getContentFromFile(annotation.file())
            .map(
                content ->
                    annotation.collection()
                        ? JsonUtility.parseAsList(content, clazz)
                        : JsonUtility.parse(content, clazz))
            .orElse(null);

    // If the data is null, proceed with the original method
    if (Objects.isNull(data)) return joinPoint.proceed();

    // If the data is a single item, return a HttpResponseEntity with the item
    if (!annotation.collection()) {
      return new HttpResponseEntity<>(annotation.status(), HttpMessageConstant.SUCCESS, data);
    }

    // If the data is a list of items, return a HttpResponseEntity with the list
    final List<?> responses = ((ArrayList) data);
    if (!annotation.pagination()) {
      return new HttpResponseEntity<>(annotation.status(), HttpMessageConstant.SUCCESS, responses);
    }

    // If the data is a list of items and pagination is required, return a HttpResponseEntity with
    // the list and pagination information
    final int totalElements = responses.size();
    PagingEntity paging = PagingEntity.builder().page(1).totalPages(1).total(totalElements).build();

    return new HttpResponseEntity<>(
        annotation.status(), HttpMessageConstant.SUCCESS, responses, paging);
  }
}
