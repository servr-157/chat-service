package com.kilo.service.chat.share.exception.user;

import com.kilo.service.chat.share.constant.DomainErrorConstants;
import com.kilo.service.chat.share.exception.DomainException;
import org.springframework.http.HttpStatus;

public class UserHttpRequestException extends DomainException {

    public UserHttpRequestException(String message) {
        super(message);
    }

    public UserHttpRequestException(DomainErrorConstants error) {
        super(error);
    }

    public UserHttpRequestException(HttpStatus httpStatus, String message) {
        super(message);
        this.setHttpStatus(httpStatus);
    }
}
