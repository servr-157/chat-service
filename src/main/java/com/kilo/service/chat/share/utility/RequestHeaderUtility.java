package com.kilo.service.chat.share.utility;

import com.kilo.service.chat.share.constant.RequestHeaderConstants;
import com.kilo.service.chat.share.entity.domain.HeaderCommand;
import com.kilo.service.chat.share.exception.HeaderNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public abstract class RequestHeaderUtility {

    /**
     * Retrieves all request headers and returns them as a map.
     *
     * @return A map containing all request headers.
     */
    public static Map<String, String> getAllHeaders() {
        Map<String, String> headers = new HashMap<>();

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes != null) {
            HttpServletRequest request = attributes.getRequest();
            Enumeration<String> headerNames = request.getHeaderNames();

            while (headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();
                headers.put(headerName, request.getHeader(headerName));
            }
        }

        return headers;
    }

    /**
     * Retrieves a specific request header.
     *
     * @param headerName The name of the header to retrieve.
     * @return The value of the specified header or null if the header is not present.
     */
    public static String getHeader(String headerName) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes != null) {
            HttpServletRequest request = attributes.getRequest();
            return request.getHeader(headerName);
        }
        return null;
    }

    /**
     * Merges multiple maps of headers into a single map.
     *
     * @param headerMaps Maps containing headers.
     * @return A single map containing merged headers.
     */
    @SafeVarargs
    public static Map<String, String> mergeHeaders(Map<String, String>... headerMaps) {
        Map<String, String> mergedHeaders = new HashMap<>();

        for (Map<String, String> headerMap : headerMaps) {
            if (headerMap != null) {
                mergedHeaders.putAll(headerMap);
            }
        }

        return mergedHeaders;
    }

    public static HeaderCommand getHeaderCommand() {
        String appID = RequestHeaderUtility.getHeader(RequestHeaderConstants.APP_ID);
        String userID = RequestHeaderUtility.getHeader(RequestHeaderConstants.USER_ID);
        String mId = RequestHeaderUtility.getHeader(RequestHeaderConstants.MERCHANT_ID);

        if (mId == null || mId.isEmpty())
            try {
                throw new HeaderNotFoundException(String.format("Header %s not found", RequestHeaderConstants.MERCHANT_ID));
            } catch (HeaderNotFoundException e) {
                throw new RuntimeException(e);
            }

        if (userID == null || userID.isEmpty())
            try {
                throw new HeaderNotFoundException(String.format("Header %s not found", RequestHeaderConstants.USER_ID));
            } catch (HeaderNotFoundException e) {
                throw new RuntimeException(e);
            }

        Long merchantID = Long.valueOf(mId);

        return HeaderCommand.of(appID, merchantID, userID);
    }
}
