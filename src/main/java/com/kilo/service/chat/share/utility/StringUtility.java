package com.kilo.service.chat.share.utility;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StringUtility {

  public static boolean isNullOrEmpty(String value) {
    return Optional.ofNullable(value).map(String::isBlank).orElse(true);
  }
}
