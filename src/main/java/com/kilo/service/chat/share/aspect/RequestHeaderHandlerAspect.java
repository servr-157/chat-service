package com.kilo.service.chat.share.aspect;

import com.kilo.service.chat.share.constant.DomainErrorConstants;
import com.kilo.service.chat.share.entity.domain.HeaderCommand;
import com.kilo.service.chat.share.exception.user.UserHttpRequestException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Slf4j
@Aspect
@Component
public class RequestHeaderHandlerAspect {

    /**
     * Define the pointcut for the methods with annotated @RequestHeaderHandler
     */
    @Pointcut(value = "@annotation(com.kiloit.kilotaskservice.share.annotion.HttpRequestHeaderHandler)")
    public void handleRequestHeader() {}

    @SneakyThrows
    @Before(value = "handleRequestHeader() && args(headerCommand,..), throwing = e")
    public void handle(JoinPoint joinPoint, HeaderCommand headerCommand) {
        /*
         * if user ID is null throw an exception
         * if merchant ID is null throw an exception
         * if app ID is null throw an exception
         * else next action
         */
        log.info("HttpRequestHeaderHandler Args ::::: {}", joinPoint.getArgs());

        if (Objects.isNull(headerCommand.getUserID()) || headerCommand.getUserID().isEmpty())
            throw new UserHttpRequestException(HttpStatus.BAD_REQUEST,
                    DomainErrorConstants.USER_ID_REQUIRED.getMessage());

        if (Objects.isNull(headerCommand.getMerchantID()))
            throw new UserHttpRequestException(HttpStatus.BAD_REQUEST,
                    DomainErrorConstants.MERCHANT_ID_REQUIRED.getMessage());

        if (Objects.isNull(headerCommand.getAppID()) || headerCommand.getAppID().isEmpty())
            throw new UserHttpRequestException(HttpStatus.BAD_REQUEST,
                    DomainErrorConstants.APP_ID_REQUIRED.getMessage());

    }
}
