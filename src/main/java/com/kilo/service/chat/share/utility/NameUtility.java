package com.kilo.service.chat.share.utility;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public abstract class NameUtility {

    public static NameParts extractFirstNameAndLastName(String fullName) {
        NameParts nameParts = new NameParts();
        String[] nameArray = fullName.split("\\s+");

        // If there is at least one word in the name
        if (nameArray.length > 0) {
            nameParts.setFirstName(nameArray[0]);

            // If there is more than one word, set the last word as the last name
            if (nameArray.length > 1) {
                nameParts.setLastName(nameArray[nameArray.length - 1]);
            }
        }

        return nameParts;
    }

    /**
     * Generates a unique name with a specified length of random letters and digits,
     * followed by a timestamp suffix.
     *
     * @param length The length of the random characters.
     * @return The generated unique name.
     */
    public static String generateUniqueNameWithSuffixTimestamp(int length) {
        String randomChars = generateRandomChars(length);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String timestamp = formatter.format(new Date());

        return randomChars + "_" + timestamp;
    }

    /**
     * Generates a random combination of letters and digits of the specified length.
     *
     * @param length The length of the random characters.
     * @return The generated random characters.
     */
    private static String generateRandomChars(int length) {
        StringBuilder randomChars = new StringBuilder();
        Random random = new Random();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (int i = 0; i < length; i++) {
            char randomChar = characters.charAt(random.nextInt(characters.length()));
            randomChars.append(randomChar);
        }

        return randomChars.toString();
    }

    @Data
    public static class NameParts {
        private String firstName;
        private String lastName;
    }

    public static String validateUsername(String username) {
        if (username == null || username.strip().isBlank()) {
            return "Username is required.";
        }

        if (username.length() < 4 || username.length() > 32) {
            return "Username must be between 4 and 32 characters.";
        }

        if (!username.matches("^[a-zA-Z0-9_]+$")) {
            return "Username must contain only letters, digits, and underscores.";
        }

        return null;
    }
}
