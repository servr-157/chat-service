package com.kilo.service.chat.share.constant;

import lombok.Getter;

@Getter
public enum DomainErrorConstants {

    ERROR("An unexpected error occurred"),
    AUTH_ERROR("Authentication error"),
    INVALID_OPT_ERROR("Invalid code"),
    EXPIRED_OPT_ERROR("Code has expired"),
    NON_EXPIRED_OPT_ERROR("Code is not within the expiration time"),
    USER_NOT_FOUND("User not found"),
    SOCIAL_AUTH_TOKEN_INVALID("Social authentication token is invalid"),
    SOCIAL_PROVIDER_USER_NOT_FOUND("User not found in the social provider"),
    UNKNOWN_ERROR(null),
    VALIDATION_ERROR("Validation error"),
    BLOCK_USER_ERROR("Error blocking user: User is already blocked."),
    UNBLOCK_USER_ERROR("Error unblocking user: User is not currently blocked."),

    AMENITY_NOT_FOUND("Amenity not found"),
    AMENITY_EXIST("Amenity already existed"),
    AMENITY_USED("Amenity already used"),

    BRAND_NOT_FOUND("Brand not found"),
    BRAND_EXIST("Brand already existed"),
    BRAND_USED("Brand already used"),

    CATEGORY_NOT_FOUND("Category not found"),
    CATEGORY_EXIST("Category already existed"),
    CATEGORY_USED("Category already used"),

    CONFIGURATION_NOT_FOUND("Configuration not found"),
    CONFIGURATION_EXIST("Configuration already existed"),
    CONFIGURATION_USED("Configuration already used"),

    PAYMENT_NOT_FOUND("Payment not found"),
    PAYMENT_EXIST("Payment already existed"),
    PAYMENT_USED("Payment already used"),

    POLICY_NOT_FOUND("Policy not found"),
    POLICY_EXIST("Policy already existed"),
    POLICY_USED("Policy already used"),

    SUPPLIER_NOT_FOUND("Supplier not found"),
    SUPPLIER_EXIST("Supplier already existed"),
    SUPPLIER_USED("Supplier already used"),

    SURROUNDING_NOT_FOUND("Surrounding not found"),
    SURROUNDING_EXIST("Surrounding already existed"),
    SURROUNDING_USED("Surrounding already used"),

    TEMPLATE_NOT_FOUND("Template not found"),
    TEMPLATE_EXIST("Template already existed"),
    TEMPLATE_USED("Template already used"),

    TEMPLATE_OPTION_NOT_FOUND("Template option not found"),
    TEMPLATE_OPTION_EXIST("Template option already existed"),
    TEMPLATE_OPTION_USED("Template option already used"),

    UNIT_NOT_FOUND("Unit not found"),
    UNIT_EXIST("Unit already existed"),
    UNIT_USED("Unit already used"),

    APP_ID_REQUIRED("The App ID is required."),
    MERCHANT_ID_REQUIRED("The Merchant ID is required."),
    USER_ID_REQUIRED("The User ID is required.");

    private final String message;

    DomainErrorConstants(String message) {
        this.message = message;
    }
}
