package com.kilo.service.chat.share.constant;

public enum DataTypeConstant {
    STRING,
    NUMERIC,
    BOOLEAN,
    DATE,
}
