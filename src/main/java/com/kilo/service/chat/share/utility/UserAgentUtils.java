package com.kilo.service.chat.share.utility;

public abstract class UserAgentUtils {

     public static String extractBrowserName(String userAgentString) {
        if (userAgentString != null && !userAgentString.isEmpty()) {
            int firstSpaceIndex = userAgentString.indexOf(' ');
            int firstSlashIndex = userAgentString.indexOf('/');

            if (firstSpaceIndex != -1 && firstSlashIndex != -1 && firstSpaceIndex < firstSlashIndex) {
                return userAgentString.substring(firstSpaceIndex + 1, firstSlashIndex).trim();
            }
        }
        return null;
    }

    public static String extractBrowserVersion(String userAgentString) {
        if (userAgentString != null && !userAgentString.isEmpty()) {
            int firstSlashIndex = userAgentString.indexOf('/');
            int firstSpaceIndex = userAgentString.indexOf(' ');

            if (firstSlashIndex != -1 && firstSpaceIndex != -1 && firstSlashIndex < firstSpaceIndex) {
                return userAgentString.substring(firstSlashIndex + 1, firstSpaceIndex).trim();
            }
        }
        return null;
    }
}
