package com.kilo.service.chat.share.constant;

public class RequestHeaderConstants {

    public static final String USER_AGENT = "User-Agent";
    public static final String PLATFORM_OS = "Platform-OS";
    public static final String PLATFORM_VERSION = "Platform-Version";
    public static final String APP_VERSION = "App-Version";
    public static final String CLIENT_IP = "Client-IP";
    public static final String ACCEPT_LANGUAGE = "Accept-Language";
    public static final String DEVICE_ID = "Device-ID";
    public static final String DEVICE_TYPE = "Device-Type";
    public static final String DEVICE_NAME = "Device-Name";
    public static final String DEVICE_MODEL = "Device-Model";
    public static final String SESSION_ID = "Session-ID";
    public static final String DEVICE_TOKEN = "Device-Token";
    public static final String APP_ID = "X-App-ID";
    public static final String MERCHANT_ID = "X-Merchant-ID";
    public static final String USER_ID = "X-User-ID";

    private RequestHeaderConstants() {
        throw new UnsupportedOperationException("Constants class - do not instantiate");
    }
}
