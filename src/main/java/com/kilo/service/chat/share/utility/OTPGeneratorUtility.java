package com.kilo.service.chat.share.utility;

import java.security.SecureRandom;

public abstract class OTPGeneratorUtility {

    private static final String NUMERIC_CHARACTERS = "0123456789";
    private static final int CODE_LENGTH = 6;

    public static String generateRandomCode() {
        SecureRandom random = new SecureRandom();
        StringBuilder randomCode = new StringBuilder(CODE_LENGTH);

        int firstDigitIndex = random.nextInt(9) + 1;
        char firstDigit = NUMERIC_CHARACTERS.charAt(firstDigitIndex);
        randomCode.append(firstDigit);

        // Generate the remaining characters
        for (int i = 1; i < CODE_LENGTH; i++) {
            int randomIndex = random.nextInt(NUMERIC_CHARACTERS.length());
            char randomChar = NUMERIC_CHARACTERS.charAt(randomIndex);
            randomCode.append(randomChar);
        }

        return randomCode.toString();
    }
}
