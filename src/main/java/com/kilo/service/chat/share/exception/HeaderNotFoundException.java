package com.kilo.service.chat.share.exception;

public class HeaderNotFoundException extends Exception {
    public HeaderNotFoundException(String message) {
        super(message);
    }
}
