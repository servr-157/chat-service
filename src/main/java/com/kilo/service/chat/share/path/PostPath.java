package com.kilo.service.chat.share.path;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PostPath {
  public static final String POST_CREATE = "/json/post/post.create.json";
  public static final String POST_UPDATE = "/json/post/post.update.json";
}
