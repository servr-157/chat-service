package com.kilo.service.chat.share.utility;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StreamUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileUtility {

  @SneakyThrows
  public static Optional<File> getClassPathFile(String path) {
    if (Objects.isNull(path) || StringUtility.isNullOrEmpty(path)) return Optional.empty();

    try {
      return Optional.of(ResourceUtils.getFile(String.format("classpath:%s", path)));
    } catch (FileNotFoundException e) {
      return Optional.empty();
    }
  }

  public static Optional<String> getContentFromFile(String path) {
    if (Objects.isNull(path) || StringUtility.isNullOrEmpty(path)) return Optional.empty();
    try (InputStream stream = new ClassPathResource(path).getInputStream()) {
      return Optional.of(StreamUtils.copyToString(stream, Charset.defaultCharset()));
    } catch (Exception ex) {
      return Optional.empty();
    }
  }

  public static String getFileExtension(String fileName) {
    int lastDotIndex = fileName.lastIndexOf(".");
    if (lastDotIndex != -1) {
      return fileName.substring(lastDotIndex + 1);
    }
    return "";
  }
}
