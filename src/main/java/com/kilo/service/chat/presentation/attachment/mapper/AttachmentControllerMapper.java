package com.kilo.service.chat.presentation.attachment.mapper;

import com.kilo.service.chat.domain.attachment.entity.Attachment;
import com.kilo.service.chat.presentation.attachment.model.request.AttachmentRequest;
import com.kilo.service.chat.presentation.attachment.model.response.AttachmentResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AttachmentControllerMapper {
    @Mapping(target = "messageId",source = "message.id")
    AttachmentResponse from(Attachment attachment);

    @Mapping(target = "message.id",source = "messageId")
    Attachment to(AttachmentRequest request);
}
