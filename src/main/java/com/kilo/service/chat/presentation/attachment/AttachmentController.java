package com.kilo.service.chat.presentation.attachment;

import com.kilo.service.chat.domain.attachment.constant.AttachmentConstant;
import com.kilo.service.chat.domain.attachment.entity.Attachment;
import com.kilo.service.chat.domain.attachment.service.AttachmentService;
import com.kilo.service.chat.presentation.attachment.mapper.AttachmentControllerMapper;
import com.kilo.service.chat.presentation.attachment.model.request.AttachmentRequest;
import com.kilo.service.chat.presentation.attachment.model.response.AttachmentResponse;
import com.kilo.service.chat.share.entity.HttpResponseEntity;
import com.kilo.service.chat.share.entity.PagingEntity;
import com.kilo.service.chat.share.entity.domain.Paging;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/attachments")
@RequiredArgsConstructor
public class AttachmentController {
    private final AttachmentService attachmentService;
    private final AttachmentControllerMapper mapper;

    @GetMapping()
    public HttpResponseEntity<List<AttachmentResponse>> list (@RequestParam(value = "filename", required = false)  String fileName,
                                                         @RequestParam(value = "type",defaultValue = "ALL", required = false) String type,
                                                         @PageableDefault(page = 1) Pageable pageable) {
        Paging<Attachment> attachmentPaging = attachmentService.filter(fileName,type.toUpperCase(),pageable.withPage(pageable.getPageNumber() - 1));
        return HttpResponseEntity.<List<AttachmentResponse>>builder()
                .data(attachmentPaging.getItems().stream().map(mapper::from).collect(Collectors.toList()))
                .paging(PagingEntity.of(attachmentPaging.getSize(),
                        attachmentPaging.getPage(),
                        attachmentPaging.getTotal(),
                        attachmentPaging.getTotalPages()))
                .build();
    }
    @GetMapping("/{id}")
    public HttpResponseEntity<AttachmentResponse> getById(@PathVariable Long id) {
        Attachment attachment = attachmentService.findById(id);
        return HttpResponseEntity.<AttachmentResponse>builder()
                .data(mapper.from(attachment))
                .build();
    }

    @PostMapping
    public HttpResponseEntity<Attachment> create(@RequestBody @Valid AttachmentRequest request) {
        return HttpResponseEntity.<Attachment>builder()
                .status(HttpStatus.CREATED.value())
                .data(attachmentService.save(mapper.to(request)))
                .build();
    }

    @DeleteMapping("/{id}")
    public HttpResponseEntity<Attachment> delete(@PathVariable Long id) {
        attachmentService.deleteById(id);
        return HttpResponseEntity.<Attachment>builder()
                .status(HttpStatus.NO_CONTENT.value())
                .message(String.format(AttachmentConstant.ATTACHMENT_ID_DELETED,id))
                .build();
    }
}
