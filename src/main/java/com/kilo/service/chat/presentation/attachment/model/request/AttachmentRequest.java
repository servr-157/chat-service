package com.kilo.service.chat.presentation.attachment.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class AttachmentRequest {
    @JsonProperty("messageID")
    @NotNull
    Long messageId;
    @NotNull
    String fileName;
    @NotNull
    String filePath;
    @NotNull
    String compressionPath;
    @Pattern(regexp = "^(IMAGE|FILE|VIDEO|AUDIO)$",message = "type mismatch one of the following value IMAGE|FILE|VIDEO|AUDIO")
    @NotNull
    String type;
}
