package com.kilo.service.chat.presentation.message.model.response;

import lombok.Data;

import java.time.Instant;

@Data
public class MessageResponse {

    Long id;
    String userId;
    String name;
    String userAvatar;
    Long conversationId;
    String content;
    Instant createdAt;
    Instant updatedAt;
}
