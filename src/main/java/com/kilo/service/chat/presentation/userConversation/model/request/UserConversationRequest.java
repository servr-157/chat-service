package com.kilo.service.chat.presentation.userConversation.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class UserConversationRequest{
    @JsonProperty("userID")
    @NotNull
    List<String> userId;
    @JsonProperty("conversationID")
    @NotNull
    Long conversationId;
    @JsonProperty("roleID")
    @NotNull
    Long roleId;
}
