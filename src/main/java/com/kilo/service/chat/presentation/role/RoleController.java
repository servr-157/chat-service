package com.kilo.service.chat.presentation.role;

import com.kilo.service.chat.domain.role.entity.Role;
import com.kilo.service.chat.domain.role.service.RoleService;
import com.kilo.service.chat.infrastructure.persistence.database.role.repository.RoleJPARepository;
import com.kilo.service.chat.presentation.role.mapper.RoleMapper;
import com.kilo.service.chat.presentation.role.model.request.RoleRequest;
import com.kilo.service.chat.presentation.role.model.respond.RoleRespond;
import com.kilo.service.chat.presentation.role.model.respond.RoleRespondDetail;
import com.kilo.service.chat.share.entity.HttpResponseEntity;
import com.kilo.service.chat.share.entity.PagingEntity;
import com.kilo.service.chat.share.entity.domain.Paging;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/role")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;
    private final RoleMapper roleMapper;

    @GetMapping
    public HttpResponseEntity<List<RoleRespond>> getAllRole(@PageableDefault(value = 10,page = 1) Pageable pageable) {
        Paging<Role> rolePaging = roleService.findAllRoles(pageable.withPage(pageable.getPageNumber()-1));
        return HttpResponseEntity.<List<RoleRespond>>builder()
                .data(rolePaging.getItems().stream().map(roleMapper::from).collect(Collectors.toList()))
                .paging(PagingEntity.of(pageable.getPageSize(),
                        rolePaging.getPage(),
                        rolePaging.getTotal(),
                        rolePaging.getTotalPages())).build();
    }

    @GetMapping("{id}")
    public HttpResponseEntity<RoleRespondDetail> getRoleById(@PathVariable Long id) {
        return HttpResponseEntity.<RoleRespondDetail>builder()
                .data(roleMapper.toDetail(roleService.findById(id))).build();
    }
    @PostMapping
    public HttpResponseEntity<Role> createRole(@RequestBody RoleRequest request) {
        return HttpResponseEntity.<Role>builder()
                .data(roleService.create(roleMapper.from(request))).build();
    }
    @DeleteMapping("{id}")
    public HttpResponseEntity<Role> deleteRole(@PathVariable Long id) {
        return HttpResponseEntity.<Role>builder()
                .data(roleService.delete(id)).build();
    }

    @PutMapping("{id}")
    public HttpResponseEntity<Role> editRole(@PathVariable Long id,@RequestBody RoleRequest request){
        return HttpResponseEntity.<Role>builder()
                .data(roleService.edite(roleMapper.from(request),id)).build();
    }
}
