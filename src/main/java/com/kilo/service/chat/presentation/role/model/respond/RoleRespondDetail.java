package com.kilo.service.chat.presentation.role.model.respond;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kilo.service.chat.infrastructure.persistence.database.permissions.entity.PermissionEntity;
import com.kilo.service.chat.infrastructure.persistence.database.userConversation.entity.UserConversationEntity;
import lombok.Data;

import java.time.Instant;
import java.util.List;
import java.util.Set;

@Data
public class RoleRespondDetail {
    Long id;
    String name;
    String code;
    Instant createdAt;
    Instant updatedAt;
    Set<UserConversationEntity> userConversations;
    @JsonProperty("permissions")
    List<PermissionEntity> permissionEntities;
}
