package com.kilo.service.chat.presentation.role.model.request;
import lombok.Data;


@Data
public class RoleRequest {
    Long id;
    String name;
    String code;
}
