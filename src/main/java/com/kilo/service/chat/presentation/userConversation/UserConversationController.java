package com.kilo.service.chat.presentation.userConversation;

import com.kilo.service.chat.domain.userConversation.constant.UserConversationConstant;
import com.kilo.service.chat.domain.userConversation.entity.UserConversation;
import com.kilo.service.chat.domain.userConversation.service.UserConversationService;
import com.kilo.service.chat.presentation.userConversation.mapper.UserConversationControllerMapper;
import com.kilo.service.chat.presentation.userConversation.model.request.UserConversationRequest;
import com.kilo.service.chat.presentation.userConversation.model.response.UserConversationResponse;
import com.kilo.service.chat.share.entity.HttpResponseEntity;
import com.kilo.service.chat.share.entity.PagingEntity;
import com.kilo.service.chat.share.entity.domain.Paging;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/user_conversations")
@RequiredArgsConstructor
public class UserConversationController {
    private final UserConversationService userConversationService;
    private final UserConversationControllerMapper mapper;

    @GetMapping
    public HttpResponseEntity<List<UserConversationResponse>> list(@RequestParam(value = "userId",required = false) String userId ,
                                                                   @RequestParam(value = "conversationId",required = false) Long conversationId ,
                                                                   @PageableDefault(page = 1) Pageable pageable) {
        Paging<UserConversation> paging = userConversationService.list(userId,conversationId,pageable.withPage(pageable.getPageNumber() - 1));
        return HttpResponseEntity.<List<UserConversationResponse>>builder()
                .data(paging.getItems().stream().map(mapper::from).collect(Collectors.toList()))
                .paging(PagingEntity.of(paging.getSize(),
                        paging.getPage(),
                        paging.getTotal(),
                        paging.getTotalPages()))
                .build();
    }

    @GetMapping("/{id}")
    public HttpResponseEntity<UserConversationResponse> getById(@PathVariable Long id){
        UserConversation userConversation = userConversationService.findById(id);
        return HttpResponseEntity.<UserConversationResponse>builder()
                .data(mapper.from(userConversation))
                .build();
    }

    @PostMapping
    public HttpResponseEntity<UserConversationResponse> create(@RequestBody @Valid UserConversationRequest request){
        UserConversation userConversation = userConversationService.save(mapper.to(request));
        return HttpResponseEntity.<UserConversationResponse>builder()
                .status(HttpStatus.CREATED.value())
                .data(mapper.from(userConversation))
                .build();
    }

    @DeleteMapping("/{id}")
    public HttpResponseEntity<Void> delete(@PathVariable Long id){
        userConversationService.deleteById(id);
        return HttpResponseEntity.<Void>builder()
                .status(HttpStatus.NO_CONTENT.value())
                .message(UserConversationConstant.USERCONVERSATION_ID_DELETED)
                .build();
    }
}
