package com.kilo.service.chat.presentation.userConversation.mapper;

import com.kilo.service.chat.domain.userConversation.entity.UserConversation;
import com.kilo.service.chat.presentation.userConversation.model.request.UserConversationRequest;
import com.kilo.service.chat.presentation.userConversation.model.response.UserConversationResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface UserConversationControllerMapper {
    @Mapping(target = "conversationId",source = "conversationId")
    @Mapping(target = "userId",source = "userId")
    UserConversationResponse from(UserConversation userConversation);

    @Mapping(target = "conversationId",source = "conversationId")
    @Mapping(target = "userId",source = "userId")
    UserConversation to(UserConversationRequest request);

    default String map(List<String> userIds) {
        return (userIds != null && !userIds.isEmpty()) ? String.join(",", userIds) : null;
    }
}
