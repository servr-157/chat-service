package com.kilo.service.chat.presentation.conversation;

import com.kilo.service.chat.domain.conversation.entity.ConversationView;
import com.kilo.service.chat.domain.conversation.service.ConversationService;
import com.kilo.service.chat.presentation.conversation.mapper.ConversationMapper;
import com.kilo.service.chat.presentation.conversation.model.request.ConversationRequest;
import com.kilo.service.chat.presentation.conversation.model.response.ConversationResponse;
import com.kilo.service.chat.share.entity.HttpResponseEntity;
import com.kilo.service.chat.share.entity.PagingEntity;
import com.kilo.service.chat.share.entity.domain.Paging;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/conversations")
@RequiredArgsConstructor
public class ConversationController {

    private final ConversationService conversationService;
    private final ConversationMapper mapper;

    @GetMapping
    public HttpResponseEntity<List<ConversationResponse>> findAll(
            @RequestParam(required = false, defaultValue = "1") String page,
            @RequestParam(required = false, defaultValue = "10") String size,
            @RequestParam(required = false) String keyword,
            @RequestParam(required = false) String type) {

        Paging<ConversationView> conversations = conversationService.findAll(
                Integer.parseInt(page),
                Integer.parseInt(size),
                keyword,
                type
        );

        return HttpResponseEntity.<List<ConversationResponse>>builder()
                .data(conversations.getItems().stream().map(mapper::from).toList())
                .paging(PagingEntity.of(
                        conversations.getSize(),
                        conversations.getPage(),
                        conversations.getTotal(),
                        conversations.getTotalPages()
                ))
                .build();
    }

    @GetMapping("list")
    public List<ConversationResponse> lists(
            @RequestParam(required = false, defaultValue = "1") String page,
            @RequestParam(required = false, defaultValue = "10") String size,
            @RequestParam(required = false) String keyword,
            @RequestParam(required = false) String type) {
        Paging<ConversationView> conversations = conversationService.findAll(
                Integer.parseInt(page),
                Integer.parseInt(size),
                keyword,
                type
        );
        return conversations.getItems().stream().map(mapper::from).toList();
    }

    @GetMapping("/{id}")
    public HttpResponseEntity<ConversationResponse> findById(@PathVariable("id") Long id) {
        return HttpResponseEntity.<ConversationResponse>builder()
                .data(mapper.from(conversationService.findById(id)))
                .build();
    }

    @PostMapping
    public HttpResponseEntity<ConversationResponse> save(@RequestBody @Valid ConversationRequest request) {
        return HttpResponseEntity.<ConversationResponse>builder()
                .data(mapper.from(conversationService.save(mapper.from(request))))
                .build();
    }

    @PutMapping("{id}")
    public HttpResponseEntity<ConversationResponse> update(@PathVariable Long id, @RequestBody @Valid ConversationRequest request) {
        return HttpResponseEntity.<ConversationResponse>builder()
                .data(mapper.from(conversationService.update(id, mapper.from(request))))
                .build();
    }

    @DeleteMapping("{id}")
    public HttpResponseEntity<ConversationResponse> delete(@PathVariable Long id) {
        return HttpResponseEntity.<ConversationResponse>builder()
                .data(mapper.from(conversationService.delete(id)))
                .build();
    }
}
