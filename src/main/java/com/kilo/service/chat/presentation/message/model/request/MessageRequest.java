package com.kilo.service.chat.presentation.message.model.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class MessageRequest {

    Long id;

    @NotNull
    Long conversationId;

    @NotBlank
    String content;
}
