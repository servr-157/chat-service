package com.kilo.service.chat.presentation.message.mapper;

import com.kilo.service.chat.domain.message.entity.Message;
import com.kilo.service.chat.presentation.message.model.request.MessageRequest;
import com.kilo.service.chat.presentation.message.model.response.MessageResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface MessageMapper {

    @Mapping(target = "conversation.id", source = "conversationId")
    Message from(MessageRequest request);

    @Mapping(target = "conversationId", source = "conversation.id")
    @Mapping(target = "userId", source = "user.userId")
    @Mapping(target = "name", source = "user.name")
    @Mapping(target = "userAvatar", source = "user.avatar")
    MessageResponse from(Message message);
}
