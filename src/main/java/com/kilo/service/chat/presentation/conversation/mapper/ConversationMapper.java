package com.kilo.service.chat.presentation.conversation.mapper;

import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.conversation.entity.ConversationView;
import com.kilo.service.chat.presentation.conversation.model.request.ConversationRequest;
import com.kilo.service.chat.presentation.conversation.model.response.ConversationResponse;
import org.mapstruct.Mapper;

@Mapper
public interface ConversationMapper {

    ConversationResponse from(Conversation conversation);

    ConversationResponse from(ConversationView conversationView);

    Conversation from(ConversationRequest request);
}
