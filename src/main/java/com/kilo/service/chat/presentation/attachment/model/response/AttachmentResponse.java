package com.kilo.service.chat.presentation.attachment.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;

@Data
public class AttachmentResponse {
    Long id;
    @JsonProperty("messageID")
    Long messageId;
    String fileName;
    String filePath;
    String compressionPath;
    String type;
    Instant createdAt;
    Instant updatedAt;
}
