package com.kilo.service.chat.presentation.userConversation.model.response;

import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class UserConversationResponse {
    Long id;
    Long conversationId;
    String userId;
    Long roleId;
    Instant createdAt;
    Instant updatedAt;
}
