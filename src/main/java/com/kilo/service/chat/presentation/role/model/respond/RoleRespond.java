package com.kilo.service.chat.presentation.role.model.respond;
import lombok.Data;

import java.time.Instant;
@Data
public class RoleRespond {
    Long id;
    String name;
    String code;
    Instant createdAt;
    Instant updatedAt;
}
