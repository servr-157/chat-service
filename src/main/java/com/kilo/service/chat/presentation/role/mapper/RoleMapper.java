package com.kilo.service.chat.presentation.role.mapper;

import com.kilo.service.chat.domain.role.entity.Role;
import com.kilo.service.chat.presentation.role.model.request.RoleRequest;
import com.kilo.service.chat.presentation.role.model.respond.RoleRespond;
import com.kilo.service.chat.presentation.role.model.respond.RoleRespondDetail;
import org.mapstruct.Mapper;

@Mapper
public interface RoleMapper {
    RoleRespond from(Role role);
    Role from(RoleRequest roleRequest);

    RoleRespondDetail toDetail(Role role);

}
