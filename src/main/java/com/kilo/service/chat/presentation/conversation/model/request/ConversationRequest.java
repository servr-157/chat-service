package com.kilo.service.chat.presentation.conversation.model.request;

import lombok.Data;

@Data
public class ConversationRequest {
    Long id;
    String title;
    String avatar;
    String type;
}
