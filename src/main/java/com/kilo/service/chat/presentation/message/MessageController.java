package com.kilo.service.chat.presentation.message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kilo.service.chat.domain.kafka.ProducerService;
import com.kilo.service.chat.domain.message.entity.Message;
import com.kilo.service.chat.domain.message.service.MessageService;
import com.kilo.service.chat.presentation.message.mapper.MessageMapper;
import com.kilo.service.chat.presentation.message.model.request.MessageRequest;
import com.kilo.service.chat.presentation.message.model.response.MessageResponse;
import com.kilo.service.chat.share.entity.HttpResponseEntity;
import com.kilo.service.chat.share.entity.PagingEntity;
import com.kilo.service.chat.share.entity.domain.Paging;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/messages")
@Slf4j
@RequiredArgsConstructor
public class MessageController {

    private final MessageService messageService;
    private final MessageMapper mapper;
    private final ProducerService producerService;
    private final ObjectMapper objectMapper;

    @GetMapping
    public HttpResponseEntity<List<MessageResponse>> getMessages(
            @RequestParam(required = false, defaultValue = "1") String page,
            @RequestParam(required = false, defaultValue = "10") String size,
            @RequestParam(required = false) Long conversationId,
            @RequestParam(required = false) String userId,
            @RequestParam(required = false) String keyword
    ){
        Paging<Message> messages = messageService.findAll(
                Integer.parseInt(page),
                Integer.parseInt(size),
                conversationId,
                userId,
                keyword
        );

        return HttpResponseEntity.<List<MessageResponse>>builder()
                .data(messages.getItems().stream().map(mapper::from).toList())
                .paging(PagingEntity.of(
                        messages.getPage(),
                        messages.getSize(),
                        messages.getTotal(),
                        messages.getTotalPages()
                ))
                .build();
    }

    @GetMapping("list")
    public List<MessageResponse> list(
            @RequestParam(required = false, defaultValue = "1") String page,
            @RequestParam(required = false, defaultValue = "10") String size,
            @RequestParam(required = false) Long conversationId,
            @RequestParam(required = false) String userId,
            @RequestParam(required = false) String keyword
    ){
        Paging<Message> messages = messageService.findAll(
                Integer.parseInt(page),
                Integer.parseInt(size),
                conversationId,
                userId,
                keyword
        );
        return messages.getItems().stream().map(mapper::from).toList();
    }

    @PostMapping
    public HttpResponseEntity<MessageResponse> sendMessage(@RequestBody @Valid MessageRequest request) {
        MessageResponse messageResponse = mapper.from(messageService.save(mapper.from(request)));
        try {
            String body = objectMapper.writeValueAsString(messageResponse);
            producerService.sendMessage(body);
            log.info("Message sent : {}", body);

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return HttpResponseEntity.<MessageResponse>builder()
                .data(messageResponse)
                .build();
    }

    @PutMapping("{id}")
    public HttpResponseEntity<MessageResponse> update(@PathVariable Long id, @RequestBody @Valid MessageRequest request) {

        MessageResponse messageResponse = mapper.from(messageService.update(id, mapper.from(request)));
        try {
            String body = objectMapper.writeValueAsString(messageResponse);
            producerService.sendMessage(body);
            log.info("Message sent : {}", body);

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return HttpResponseEntity.<MessageResponse>builder()
                .data(messageResponse)
                .build();
    }

    @DeleteMapping("{id}")
    public HttpResponseEntity<MessageResponse> delete(@PathVariable Long id) {
        MessageResponse messageResponse = mapper.from(messageService.delete(id));
        try {
            String body = objectMapper.writeValueAsString(messageResponse);
            producerService.sendMessage(body);
            log.info("Message sent : {}", body);

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return HttpResponseEntity.<MessageResponse>builder()
                .data(messageResponse)
                .build();
    }

}
