package com.kilo.service.chat.presentation.conversation.model.response;

import lombok.Data;

import java.time.Instant;

@Data
public class ConversationResponse {
    Long id;
    String title;
    String avatar;
    String lastMessage;
    Instant createdAt;
    Instant updatedAt;
    String createdBy;
    String type;
}
