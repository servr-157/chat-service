package com.kilo.service.chat.domain.conversation.constant;

public class ConversationConstant {
    public static final String CONVERSATION_ID_NOT_FOUND = "Conversation id: %s not found";
    public static final String CONVERSATION_ID_IS_PRIVATE = "Conversation id: %s is private";
}
