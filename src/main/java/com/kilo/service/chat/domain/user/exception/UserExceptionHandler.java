package com.kilo.service.chat.domain.user.exception;

import com.kilo.service.chat.share.advise.ApiControllerExceptionHandler;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

public class UserExceptionHandler extends ApiControllerExceptionHandler {

    public UserExceptionHandler(Environment env) {
        super(env);
    }

    @ExceptionHandler(UserNotFountException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> userNotFountException(UserNotFountException exception) {
        return this.getBadRequestErrorElement(exception.getStackTrace(), exception.getClass().getName(), exception.getMessage(), exception);
    }
}
