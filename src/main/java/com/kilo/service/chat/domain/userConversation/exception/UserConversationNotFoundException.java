package com.kilo.service.chat.domain.userConversation.exception;

import com.kilo.service.chat.share.constant.DomainErrorConstants;
import com.kilo.service.chat.share.exception.DomainException;
import org.springframework.http.HttpStatus;

public class UserConversationNotFoundException extends DomainException {
    public UserConversationNotFoundException(String message){
        super(message);
    }
    public UserConversationNotFoundException(DomainErrorConstants error){
        super(error);
    }
    public UserConversationNotFoundException(HttpStatus httpStatus, String message){
        super(httpStatus, message);
    }
}
