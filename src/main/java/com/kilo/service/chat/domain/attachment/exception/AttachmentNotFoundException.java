package com.kilo.service.chat.domain.attachment.exception;

import com.kilo.service.chat.share.constant.DomainErrorConstants;
import com.kilo.service.chat.share.exception.DomainException;
import org.springframework.http.HttpStatus;

public class AttachmentNotFoundException extends DomainException {
    public AttachmentNotFoundException(String message){
        super(message);
    }
    public AttachmentNotFoundException(DomainErrorConstants error){
        super(error);
    }
    public AttachmentNotFoundException(HttpStatus httpStatus, String message){
        super(httpStatus, message);
    }
}
