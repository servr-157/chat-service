package com.kilo.service.chat.domain.attachment.entity;

import com.kilo.service.chat.infrastructure.persistence.database.message.entity.MessageEntity;
import lombok.Data;

import java.time.Instant;

@Data
public class Attachment {
    Long id;
    String fileName;
    String filePath;
    String compressionPath;
    MessageEntity message;
    String type;
    Instant createdAt;
    Instant updatedAt;
    Instant deletedAt;
}
