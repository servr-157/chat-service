package com.kilo.service.chat.domain.user.repository;

import com.kilo.service.chat.domain.user.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    Optional<User> findById(String userId);

    void save(User user);

    List<User> findByIds(List<String> userIds);
}
