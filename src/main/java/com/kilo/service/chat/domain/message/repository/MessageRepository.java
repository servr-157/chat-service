package com.kilo.service.chat.domain.message.repository;

import com.kilo.service.chat.domain.message.entity.Message;
import com.kilo.service.chat.share.entity.domain.Paging;

import java.util.Optional;

public interface MessageRepository {

    Paging<Message> findAll(int page, int size, Long conversationId, String userId, String keyword);

    Message save(Message message);

    Optional<Message> findById(Long id);

    Boolean isOwnMessage(String userId, Long messageId);

}
