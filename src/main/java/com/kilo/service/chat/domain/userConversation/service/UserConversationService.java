package com.kilo.service.chat.domain.userConversation.service;

import com.kilo.service.chat.domain.userConversation.entity.UserConversation;
import com.kilo.service.chat.share.entity.domain.Paging;
import org.springframework.data.domain.Pageable;

public interface UserConversationService {
    UserConversation save(UserConversation userConversation);

    UserConversation findById(Long id);

    void deleteById(Long id);

    Paging<UserConversation> list(String userId, Long conversationId, Pageable pageable);
}
