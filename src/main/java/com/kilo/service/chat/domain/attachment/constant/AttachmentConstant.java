package com.kilo.service.chat.domain.attachment.constant;

public class AttachmentConstant {
    public static final String ID_NOT_FOUND = "Attachment with id %s not found";
    public static final String ATTACHMENT_ID_DELETED = "Attachment id %s has been deleted";
}
