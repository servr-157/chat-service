package com.kilo.service.chat.domain.userConversation.constant;

public class UserConversationConstant {
    public static final String ID_NOT_FOUND = "User conversation with id %s not found";
    public static final String USERCONVERSATION_ID_DELETED = "User conversation id %s has been deleted";
}
