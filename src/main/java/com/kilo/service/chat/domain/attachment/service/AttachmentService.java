package com.kilo.service.chat.domain.attachment.service;

import com.kilo.service.chat.domain.attachment.entity.Attachment;
import com.kilo.service.chat.infrastructure.persistence.database.column.AttachmentTypeEnum;
import com.kilo.service.chat.share.entity.domain.Paging;
import org.springframework.data.domain.Pageable;

public interface AttachmentService {
    Attachment save(Attachment attachment);
    Attachment findById(Long id);
    void deleteById(Long id);
    Paging<Attachment> filter(String fileName, String type, Pageable pageable);
}
