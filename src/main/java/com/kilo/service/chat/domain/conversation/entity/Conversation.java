package com.kilo.service.chat.domain.conversation.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;

@Data
@NoArgsConstructor
public class Conversation implements Serializable {
    Long id;
    String title;
    String avatar;
    String lastMessage;
    String type;
    String createdBy;
    String updatedBy;
    Instant createdAt;
    Instant updatedAt;
    Instant deletedAt;

    public Conversation(Long id, String title, String avatar, String lastMessage, String type, String createdBy, String updatedBy, Instant createdAt, Instant updatedAt, Instant deletedAt) {
        this.id = id;
        this.title = title;
        this.avatar = avatar;
        this.lastMessage = lastMessage;
        this.type = type;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }
}