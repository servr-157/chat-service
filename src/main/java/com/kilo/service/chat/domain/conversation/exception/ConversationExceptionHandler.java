package com.kilo.service.chat.domain.conversation.exception;

import com.kilo.service.chat.share.advise.ApiControllerExceptionHandler;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ConversationExceptionHandler extends ApiControllerExceptionHandler {

    public ConversationExceptionHandler(Environment env) {
        super(env);
    }

    @ExceptionHandler(ConversationNotFountException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> conversationNotFountException(ConversationNotFountException exception) {
        return this.getBadRequestErrorElement(exception.getStackTrace(), exception.getClass().getName(), exception.getMessage(), exception);
    }
}
