package com.kilo.service.chat.domain.role.service;

import com.kilo.service.chat.domain.role.entity.Role;
import com.kilo.service.chat.share.entity.domain.Paging;
import org.springframework.data.domain.Pageable;

public interface RoleService {
    Paging<Role> findAllRoles(Pageable pageable);
    Role findById(Long id);
    Role create(Role role);
    Role edite(Role role,Long id);
    Role delete(Long id);
}
