package com.kilo.service.chat.domain.message.exception;

import com.kilo.service.chat.share.constant.DomainErrorConstants;
import com.kilo.service.chat.share.exception.DomainException;
import org.springframework.http.HttpStatus;

public class MessageInvalidException extends DomainException {

    public MessageInvalidException(String message) {
        super(message);
        this.setHttpStatus(HttpStatus.BAD_REQUEST);
    }

    public MessageInvalidException(DomainErrorConstants error) {
        super(error);
    }

    public MessageInvalidException(HttpStatus httpStatus, String message) {
        super(message);
        this.setHttpStatus(httpStatus);
    }
}
