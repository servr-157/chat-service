package com.kilo.service.chat.domain.message.constant;

public class MessageConstant {
    public static final String MESSAGE_ID_NOT_FOUND = "Message ID: %s not found";
    public static final String MESSAGE_INVALID = "Message Id: %s is not your message";
}
