package com.kilo.service.chat.domain.message.entity;

import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.user.entity.User;
import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

@Data
public class Message implements Serializable {

    Long id;
    Instant createdAt;
    Instant updatedAt;
    Instant deletedAt;
    String content;
    Conversation conversation = new Conversation();
    User user = new User();

}