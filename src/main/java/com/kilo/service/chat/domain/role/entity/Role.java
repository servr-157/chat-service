package com.kilo.service.chat.domain.role.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class Role {
    Long id;
    String name;
    String code;
    Instant createdAt;
    Instant updatedAt;
    Instant deletedAt;
}
