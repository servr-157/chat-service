package com.kilo.service.chat.domain.userConversation.exception;

import com.kilo.service.chat.share.advise.ApiControllerExceptionHandler;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class UserConversationExceptionHandler extends ApiControllerExceptionHandler {
    public UserConversationExceptionHandler(Environment env) {super(env);}

    @ExceptionHandler(UserConversationNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> UserConversationNotFoundException(UserConversationNotFoundException ex){
        return this.getBadRequestErrorElement(ex.getStackTrace(), ex.getClass().getName(),ex.getMessage(),ex);
    }
}
