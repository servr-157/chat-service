package com.kilo.service.chat.domain.conversation.exception;

import com.kilo.service.chat.share.constant.DomainErrorConstants;
import com.kilo.service.chat.share.exception.DomainException;
import org.springframework.http.HttpStatus;

public class ConversationNotFountException extends DomainException {

    public ConversationNotFountException(String message){
        super(message);
        this.setHttpStatus(HttpStatus.BAD_REQUEST);
    }

    public ConversationNotFountException(DomainErrorConstants error){
        super(error);
    }

    public ConversationNotFountException(HttpStatus httpStatus, String message){
        super(message);
        this.setHttpStatus(httpStatus);
    }
}
