package com.kilo.service.chat.domain.attachment.repository;

import com.kilo.service.chat.domain.attachment.entity.Attachment;
import com.kilo.service.chat.share.entity.domain.Paging;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface AttachmentRepository {
    Attachment save(Attachment attachment);
    Optional<Attachment> findById(Long id);
    Paging<Attachment> filter(String fileName, String type, Pageable pageable);
}
