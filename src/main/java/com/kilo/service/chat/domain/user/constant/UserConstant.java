package com.kilo.service.chat.domain.user.constant;

public class UserConstant {
    public static final String USER_ID_NOT_FOUND = "User id: %s not found";
}
