package com.kilo.service.chat.domain.userConversation.repository;

import com.kilo.service.chat.domain.userConversation.entity.UserConversation;
import com.kilo.service.chat.share.entity.domain.Paging;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface UserConversationRepository {

    UserConversation save(UserConversation userConversation);

    Optional<UserConversation> findById(Long id);

    Boolean existsUserInConversation(String userId, Long conversationId);

    Paging<UserConversation> list(String userId,Long conversationId,Pageable pageable);
}
