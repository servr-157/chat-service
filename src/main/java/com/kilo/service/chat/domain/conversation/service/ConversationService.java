package com.kilo.service.chat.domain.conversation.service;

import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.conversation.entity.ConversationView;
import com.kilo.service.chat.share.entity.domain.Paging;

public interface ConversationService {

    Paging<ConversationView> findAll(int page, int size, String keyword, String type);

    Conversation save(Conversation conversation);

    Conversation update(Long id, Conversation conversation);

    Conversation delete(Long id);

    ConversationView findById(Long id);
}
