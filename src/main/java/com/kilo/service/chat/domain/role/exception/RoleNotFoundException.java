package com.kilo.service.chat.domain.role.exception;

import com.kilo.service.chat.share.exception.DomainException;
import org.springframework.http.HttpStatus;

public class RoleNotFoundException extends DomainException {
    public RoleNotFoundException(String message) {
        super(message);
    }
    public RoleNotFoundException(String message, HttpStatus httpStatus) {
        super(message);
        this.setHttpStatus(httpStatus);
    }
}
