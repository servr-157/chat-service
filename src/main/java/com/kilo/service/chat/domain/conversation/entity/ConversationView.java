package com.kilo.service.chat.domain.conversation.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.Instant;


@Data
public class ConversationView implements Serializable {
    Long id;
    String userId;
    String title;
    String type;
    String avatar;
    String lastMessage;
    Instant createdAt;
    String createdBy;
    Instant deletedAt;
    Instant updatedAt;
    String updatedBy;
}