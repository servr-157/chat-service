package com.kilo.service.chat.domain.conversation.repository;

import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.conversation.entity.ConversationView;
import com.kilo.service.chat.share.entity.domain.Paging;

import java.util.Optional;

public interface ConversationRepository {

    Paging<ConversationView> findAll(int page, int size, String keyword, String type, String userId);

    Optional<Conversation> findById(Long id);

    Optional<ConversationView> findByConversationId(String userId, Long conversationId);

    Conversation save(Conversation conversation);
}
