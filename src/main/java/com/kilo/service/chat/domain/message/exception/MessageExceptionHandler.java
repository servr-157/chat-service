package com.kilo.service.chat.domain.message.exception;

import com.kilo.service.chat.share.advise.ApiControllerExceptionHandler;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

//@RestControllerAdvice
public class MessageExceptionHandler extends ApiControllerExceptionHandler {

    public MessageExceptionHandler(Environment env) {
        super(env);
    }

    @ExceptionHandler(MessageNotFountException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> messageNotFountException(MessageNotFountException e) {
        return this.getBadRequestErrorElement(e.getStackTrace(), e.getClass().getName(), e.getMessage(), e);
    }

    @ExceptionHandler(MessageInvalidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> messageInvalidException(MessageInvalidException e) {
        return this.getBadRequestErrorElement(e.getStackTrace(), e.getClass().getName(), e.getMessage(), e);
    }
}
