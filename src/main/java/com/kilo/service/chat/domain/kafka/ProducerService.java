package com.kilo.service.chat.domain.kafka;

public interface ProducerService {

    void sendMessage(String message);
}
