package com.kilo.service.chat.domain.role.repository;

import com.kilo.service.chat.domain.role.entity.Role;
import com.kilo.service.chat.share.entity.domain.Paging;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface RoleRepository {
    Paging<Role> findAll( Pageable pageable);
    Optional<Role> findById(Long id);
    Role save(Role role);
    boolean existsByNames(String name);
}