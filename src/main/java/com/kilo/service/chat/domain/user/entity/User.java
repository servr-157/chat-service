package com.kilo.service.chat.domain.user.entity;

import com.kilo.service.chat.infrastructure.persistence.database.user.entity.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;

/**
 * DTO for {@link UserEntity}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    String userId;
    String name;
    String avatar;
    Instant createdAt;
    Instant updatedAt;
    Instant deletedAt;
}