package com.kilo.service.chat.domain.message.exception;

import com.kilo.service.chat.share.constant.DomainErrorConstants;
import com.kilo.service.chat.share.exception.DomainException;
import org.springframework.http.HttpStatus;

public class MessageNotFountException extends DomainException {

    public MessageNotFountException(String message) {
        super(message);
        this.setHttpStatus(HttpStatus.NOT_FOUND);
    }

    public MessageNotFountException(DomainErrorConstants error){
        super(error);
    }

    public MessageNotFountException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }
}
