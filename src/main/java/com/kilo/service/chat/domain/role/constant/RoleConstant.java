package com.kilo.service.chat.domain.role.constant;


public class RoleConstant {
    public final static String EXIST_ROLE_NAME = "Role name:'%s' is exist";
    public final static String ROLE_NOT_FOUND = "Role with id: %s is not found";
}
