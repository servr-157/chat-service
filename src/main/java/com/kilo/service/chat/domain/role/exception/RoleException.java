package com.kilo.service.chat.domain.role.exception;

import com.kilo.service.chat.share.exception.DomainException;
import org.springframework.http.HttpStatus;

public class RoleException extends DomainException {

    private RoleException(String message) {
        super(message);
        this.setHttpStatus(HttpStatus.NOT_FOUND);
    }

    public static RoleException exist() {
        return new RoleException("The user is exist %s");
    }
}
