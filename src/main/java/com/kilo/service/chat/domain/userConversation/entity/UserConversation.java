package com.kilo.service.chat.domain.userConversation.entity;

import com.kilo.service.chat.domain.conversation.entity.Conversation;
import com.kilo.service.chat.domain.role.entity.Role;
import com.kilo.service.chat.domain.user.entity.User;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class UserConversation {
    Long id;
    List<String> userId;
    Long conversationId;
    Long roleId;
    Instant createdAt;
    Instant updatedAt;
    Instant deletedAt;
    User user;
    Conversation conversations;
    Role role;
}
