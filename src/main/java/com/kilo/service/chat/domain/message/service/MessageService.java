package com.kilo.service.chat.domain.message.service;

import com.kilo.service.chat.domain.message.entity.Message;
import com.kilo.service.chat.share.entity.domain.Paging;

public interface MessageService {
    Paging<Message> findAll(int page, int size, Long conversationId,String userId, String keyword);

    Message save(Message message);

    Message update(Long id, Message message);

    Message delete(Long id);

}
