package com.kilo.service.chat.domain.user.exception;

import com.kilo.service.chat.share.constant.DomainErrorConstants;
import com.kilo.service.chat.share.exception.DomainException;
import org.springframework.http.HttpStatus;

public class UserNotFountException extends DomainException {

    public UserNotFountException(String message) {
        super(message);
        this.setHttpStatus(HttpStatus.NOT_FOUND);
    }

    public UserNotFountException(DomainErrorConstants error) {
        super(error);
    }

    public UserNotFountException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }
}
